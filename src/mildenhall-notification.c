/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include "mildenhall-notification.h"


G_DEFINE_BOXED_TYPE (MildenhallNotification, mildenhall_notification,
                     mildenhall_notification_copy, mildenhall_notification_free)


MildenhallNotification *
mildenhall_notification_new (MildenhallNotificationManager *manager,
                             CbyProcessInfo *process_info,
                             const gchar *app_icon, const gchar *summary,
                             const gchar *body,
                             const gchar * const *arg_actions, GVariant *hints,
                             guint id, gint expire_timeout,
                             GDBusMethodInvocation *notify_invocation,
                             GTask *close_task)
{
  g_autoptr (MildenhallNotification) notification = NULL;

  notification = g_new0 (MildenhallNotification, 1);
  if (manager != NULL)
    notification->nm_ref = g_object_ref (manager);
  if (process_info != NULL)
    notification->process_info = g_object_ref (process_info);
  notification->app_icon = g_strdup (app_icon);
  notification->summary = g_strdup (summary);
  notification->body = g_strdup (body);
  notification->actions = g_strdupv ((gchar **) arg_actions);
  notification->hints = g_variant_ref (hints);
  notification->id = id;
  notification->expire_timeout = expire_timeout;
  if (notify_invocation != NULL)
    notification->notify_invocation = g_object_ref (notify_invocation);
  if (close_task != NULL)
    notification->close_task = g_object_ref (close_task);

  return g_steal_pointer (&notification);
}

MildenhallNotification *
mildenhall_notification_copy (const MildenhallNotification *notification)
{
  g_return_val_if_fail (notification != NULL, NULL);

  return mildenhall_notification_new (notification->nm_ref,
                                      notification->process_info,
                                      notification->app_icon,
                                      notification->summary,
                                      notification->body,
                                      (const gchar * const *) notification->actions,
                                      notification->hints,
                                      notification->id,
                                      notification->expire_timeout,
                                      notification->notify_invocation,
                                      notification->close_task);
}

void
mildenhall_notification_free (MildenhallNotification *notification)
{
  g_return_if_fail (notification != NULL);

  g_clear_object (&notification->nm_ref);
  g_clear_object (&notification->process_info);
  g_free (notification->app_icon);
  g_free (notification->summary);
  g_free (notification->body);
  g_strfreev (notification->actions);
  g_variant_unref (notification->hints);
  g_clear_object (&notification->notify_invocation);
  g_clear_object (&notification->close_task);

  g_free (notification);
}
