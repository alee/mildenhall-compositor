/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_NOTIFICATION_MANAGER_H__
#define __MILDENHALL_NOTIFICATION_MANAGER_H__

#include <glib-object.h>
#include <meta/meta-plugin.h>
#include <meta/window.h>
#include <meta/theme.h>
#include <meta/display.h>
#include <meta/util.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_NOTIFICATION_MANAGER (mildenhall_notification_manager_get_type ())
G_DECLARE_FINAL_TYPE (MildenhallNotificationManager, mildenhall_notification_manager, MILDENHALL,
                      NOTIFICATION_MANAGER, GObject)

MildenhallNotificationManager *mildenhall_notification_manager_new (MetaPlugin *plugin);

G_END_DECLS

#endif /* __MILDENHALL_NOTIFICATION_MANAGER_H__ */
