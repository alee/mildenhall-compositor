/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_NOTIFICATION_H__
#define __MILDENHALL_NOTIFICATION_H__

#include <glib.h>
#include <canterbury/canterbury.h>
#include "mildenhall-notification-manager.h"

G_BEGIN_DECLS

typedef struct
{
  CbyProcessInfo *process_info;
  gchar *app_icon;
  gchar *summary;
  gchar *body;
  gchar **actions;
  GVariant *hints;
  guint id;
  gint expire_timeout; /* in seconds */
  GDBusMethodInvocation *notify_invocation;
  GTask *close_task;
  MildenhallNotificationManager *nm_ref;
} MildenhallNotification;

#define MILDENHALL_TYPE_NOTIFICATION (mildenhall_notification_get_type ())

MildenhallNotification *mildenhall_notification_new (MildenhallNotificationManager *manager,
                                                     CbyProcessInfo *process_info,
                                                     const gchar *app_icon,
                                                     const gchar *summary,
                                                     const gchar *body,
                                                     const gchar * const *arg_actions,
                                                     GVariant *hints,
                                                     guint id,
                                                     gint expire_timeout,
                                                     GDBusMethodInvocation *notify_invocation,
                                                     GTask *close_task);

MildenhallNotification *mildenhall_notification_copy (const MildenhallNotification *notification);
void mildenhall_notification_free (MildenhallNotification *notification);

GType mildenhall_notification_get_type (void);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (MildenhallNotification, mildenhall_notification_free)

G_END_DECLS

#endif /* !MILDENHALL_NOTIFICATION_H */
