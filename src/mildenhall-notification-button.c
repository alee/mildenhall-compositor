/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-notification-button.h"

typedef struct _MildenhallNotificationButton
{
  LightwoodButton parent;
} MildenhallNotificationButton;

G_DEFINE_TYPE (MildenhallNotificationButton, mildenhall_notification_button,
               LIGHTWOOD_TYPE_BUTTON);

static void
mildenhall_notification_button_class_init (MildenhallNotificationButtonClass *klass)
{
}

static void
mildenhall_notification_button_init (MildenhallNotificationButton *self)
{
}

MildenhallNotificationButton *
mildenhall_notification_button_new (gfloat width, gfloat height)
{
  return g_object_new (MILDENHALL_TYPE_NOTIFICATION_BUTTON,
                       "swipe-enabled", TRUE,
                       "reactive", TRUE,
                       "long-press-enabled", TRUE,
                       "width", width,
                       "height", height,
                       NULL);
}
