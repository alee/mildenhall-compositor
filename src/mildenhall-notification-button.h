/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_NOTIFICATION_BUTTON_H__
#define __MILDENHALL_NOTIFICATION_BUTTON_H__

#include <glib-object.h>
#include "liblightwood-button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_NOTIFICATION_BUTTON (mildenhall_notification_button_get_type ())
G_DECLARE_FINAL_TYPE (MildenhallNotificationButton, mildenhall_notification_button, MILDENHALL,
                      NOTIFICATION_BUTTON, LightwoodButton)

MildenhallNotificationButton *mildenhall_notification_button_new (gfloat width, gfloat height);

G_END_DECLS

#endif /* __MILDENHALL_NOTIFICATION_BUTTON_H__ */


