/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-notification-window.h"
#include "mildenhall-notification-button.h"
#include <clutter/clutter.h>
#include <glib.h>
#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include "enumtypes.h"

typedef struct _MildenhallNotificationWindow
{
  ClutterActor parent;

  ClutterActor *body; /* unowned */
  gint timeout;
  MildenhallNotificationState state;
  MildenhallNotificationClosedReason reason;
  guint source_id;
  guint notification_id;
  ClutterActor *pressed_img; /* unowned */
  ClutterActor *released_img; /* unowned */
  MildenhallNotification *notification; /* owned */
} MildenhallNotificationWindow;

static void
mildenhall_notification_window_initable_init (GInitableIface *iface);

static void
notification_transition_completed_cb (ClutterAnimation *anim,
                                      MildenhallNotificationWindow *self);

G_DEFINE_TYPE_WITH_CODE (MildenhallNotificationWindow, mildenhall_notification_window,
    CLUTTER_TYPE_ACTOR,
    G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE, mildenhall_notification_window_initable_init))

#define WINDOW_POS 79.0, 460.0
#define EASING_DURATION_MS 750
#define HEAD_BAR_SIZE  648.0,8.0
#define HEAD_BAR_POSITION  0.0,0.0
#define TEXT_BG_COLOUR "#000000ff"
#define TEXT_BG_SIZE 648.0,100.0
#define TEXT_BG_POS  0.0,8.0
#define TEXT_BG_OPACITY 204.0
#define LEFT_BORDER_SIZE 1.0,96.0
#define LEFT_BORDER_POS 57.0, 10.0
#define LEFT_BORDER_OPACITY 51.0
#define RIGHT_BORDER_SIZE 1.0,96.0
#define RIGHT_BORDER_POS 589.0, 10.0
#define RIGHT_BORDER_OPACITY 51.0
#define BETWEEN_BAR_SIZE 648.0,10.0
#define BETWEEN_BAR_POS 0.0,108.0
#define BETWEEN_BAR_OPACITY 51.0
#define TEXT_FONT "Deja Vu Sans 28px"
#define BUTTON_TEXT_FONT "Deja Vu Sans 32px"
#define COLOUR_ENABLE "#FFFFFFFF"
#define COLOUR_DISABLE "#98A9B2FF"
#define BODY_TEXT_POS  60.0, 20.0
#define BODY_TEXT_SIZE  524,100
#define ICON_SIZE 36.0,36.0
#define ICON_OPACITY 0x33
#define APP_ICON_POS 10.0,38.0
#define MSG_ICON_POS 601.0,38.0
#define BUTTON_HEIGHT 54.0
#define SINGLE_BUTTON_SIZE 648.0, BUTTON_HEIGHT
#define SINGLE_BUTTON_POS 0.0, 118.0
#define SINGLE_BUTTON_TEXT_XOFFSET 324
#define SINGLE_BUTTON_TEXT_YOFFSET 160.0
#define DOUBLE_BUTTON_BUTTON1_POS 0.0, 118.0
#define DOUBLE_BUTTON_BUTTON1_SIZE 323.0, BUTTON_HEIGHT
#define DOUBLE_BUTTON_BUTTON1_XOFFSET  161
#define DOUBLE_BUTTON_BUTTON1_YOFFSET  160
#define DOUBLE_BUTTON_BUTTON2_POS 325.0, 118.0
#define DOUBLE_BUTTON_BUTTON2_SIZE 323.0, BUTTON_HEIGHT
#define DOUBLE_BUTTON_BUTTON2_XOFFSET 486.0
#define DOUBLE_BUTTON_BUTTON2_YOFFSET 160

enum _NotificationProperty
{
  PROP_0,
  PROP_NOTIFICATION_STATE,
  PROP_NOTIFICATION_ID,
  PROP_NOTIFICATION_STRUCT,
  PROP_LAST
};

enum _NotificationSignals
{
  SIG_NOTIFICATION_NONE,
  SIG_NOTIFICATION_ACTION, /* Emitted when action  is performed on popup (ex:popup-button press)*/
  SIG_NOTIFICATION_NUM_SIGNALS
};

static guint32 notification_signals[SIG_NOTIFICATION_NUM_SIGNALS] =
  { 0, };

static GParamSpec *properties[PROP_LAST];

static gboolean
ui_texture_update_from_file (ClutterActor *image_actor, GFile *file,
                             gint width, gint height, GError **error)
{
  GdkPixbuf *pixbuf = NULL;
  gchar *path = NULL;
  ClutterContent *texture = NULL;

  path = g_file_get_path (file);
  g_return_val_if_fail (path, FALSE);

  /* if width/height is provided as 0, image will get created with original image size */
  if (width == 0 || height == 0)
    {
      pixbuf = gdk_pixbuf_new_from_file (path, error);
    }
  else
    {
      /* if width/height is non-zero, image will get scaled to given width/height */
      pixbuf = gdk_pixbuf_new_from_file_at_scale (path, width, height, FALSE,
                                                  error);
    }
  /* if pixbuf is null/ return */
  if (pixbuf == NULL)
    {
      g_free (path);
      return FALSE;
    }
  texture = clutter_actor_get_content (image_actor);
  if (!clutter_image_set_data (
      CLUTTER_IMAGE (texture),
      gdk_pixbuf_get_pixels (pixbuf),
      gdk_pixbuf_get_has_alpha (pixbuf) ?
          COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
      gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf),
      gdk_pixbuf_get_rowstride (pixbuf), error))
    {
      g_clear_object (&pixbuf);
      g_free (path);
      return FALSE;
    }

  /* update the content box with image size and set content to it */
  clutter_actor_set_size (image_actor, gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf));
  g_clear_object (&pixbuf);
  g_free (path);
  return TRUE;
}

static gboolean
ui_texture_set_from_file (ClutterActor *image_actor, const gchar *file_path,
                          gint width, gint height, GError **error)
{
  GFile *file = NULL;
  gboolean ret_val;

  g_return_val_if_fail (image_actor != NULL, FALSE);
  g_return_val_if_fail (file_path != NULL, FALSE);
  file = g_file_new_for_path (file_path);
  ret_val = ui_texture_update_from_file (image_actor, file, width, height, error);
  g_clear_object (&file);
  return ret_val;
}

static ClutterActor *
create_texture_from_file (const gchar *file_path, gint width, gint height,
                          GError **error)
{
  ClutterActor *box = NULL;
  ClutterContent *texture = NULL;
  gboolean ret_val;

  g_return_val_if_fail (file_path != NULL, NULL);
  /* box for image content */
  box = clutter_actor_new ();
  texture = clutter_image_new ();
  clutter_actor_set_content (box, texture);
  ret_val = ui_texture_set_from_file (box, file_path, width, height, error);
  g_clear_object (&texture);
  if(!ret_val)
    {
      clutter_actor_destroy (box);
      return NULL;
    }
  return box;
}

static void
notification_transition_completed_cb (ClutterAnimation *anim,
                                      MildenhallNotificationWindow *self)
{
  if (self->state == MILDENHALL_NOTIFICATION_SHOWING)
    {
      self->state = MILDENHALL_NOTIFICATION_SHOWN;
    }
  else if (self->state == MILDENHALL_NOTIFICATION_HIDING)
    {
      self->state = MILDENHALL_NOTIFICATION_NONE;
    }
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NOTIFICATION_STATE]);
}

static void
mildenhall_notification_window_get_property (GObject *object, guint property_id,
                                             GValue *value, GParamSpec *pspec)
{
  MildenhallNotificationWindow *self = MILDENHALL_NOTIFICATION_WINDOW (object);

  switch (property_id)
    {
    case PROP_NOTIFICATION_STATE:
      g_value_set_enum (value, self->state);
      break;
    case PROP_NOTIFICATION_ID:
      g_value_set_uint (value, self->notification_id);
      break;
    case PROP_NOTIFICATION_STRUCT:
      g_value_set_boxed (value, self->notification);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_notification_window_set_property (GObject *object, guint prop_id,
                                             const GValue *value,
                                             GParamSpec *pspec)
{
  MildenhallNotificationWindow *self = MILDENHALL_NOTIFICATION_WINDOW (object);
  MildenhallNotification *notification = NULL;

  switch (prop_id)
    {
    case PROP_NOTIFICATION_ID:
      self->notification_id = g_value_get_uint (value);
      break;
    case PROP_NOTIFICATION_STRUCT:
      notification = g_value_get_boxed (value);
      self->notification = mildenhall_notification_copy (notification);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
mildenhall_notification_window_set_body_text (
    MildenhallNotificationWindow *self, const gchar *body)
{
  ClutterColor color_enable;

  clutter_color_from_string (&color_enable, COLOUR_ENABLE);
  self->body = clutter_text_new ();
  clutter_text_set_font_name (CLUTTER_TEXT (self->body),
                              TEXT_FONT);
  clutter_text_set_use_markup (CLUTTER_TEXT (self->body), TRUE);
  clutter_text_set_markup (CLUTTER_TEXT (self->body), body);
  clutter_text_set_color (CLUTTER_TEXT (self->body), &color_enable);
  clutter_text_set_justify (CLUTTER_TEXT (self->body), TRUE);
  clutter_text_set_line_alignment (CLUTTER_TEXT (self->body),
                                   PANGO_ALIGN_CENTER);
  clutter_text_set_ellipsize (CLUTTER_TEXT (self->body),
                              PANGO_ELLIPSIZE_END);
  clutter_actor_set_size (self->body, BODY_TEXT_SIZE);
  clutter_text_set_line_wrap (CLUTTER_TEXT (self->body), TRUE);
  clutter_text_set_line_wrap_mode (CLUTTER_TEXT (self->body),
                                   PANGO_WRAP_CHAR);
  clutter_actor_set_position (self->body, BODY_TEXT_POS);
  clutter_actor_add_child (CLUTTER_ACTOR (self), self->body);
}

static gboolean
mildenhall_notification_window_set_hints (MildenhallNotificationWindow *self,
                                          GVariant *hints, GError **error)
{
  GVariantIter iter;
  GVariant *out_variant;
  gchar *hint_name;

  g_variant_iter_init (&iter, hints);
  while (g_variant_iter_next (&iter, "{sv}", &hint_name, &out_variant))
    {
      if (!g_strcmp0 (hint_name, "image-path"))
        {
          const gchar *img_path;
          ClutterActor *msg_icon = NULL;

          img_path = g_variant_get_string (out_variant, NULL);
          msg_icon = create_texture_from_file (img_path, ICON_SIZE, error);
          if (error && *error)
            {
              g_free (hint_name);
              g_variant_unref (out_variant);
              return FALSE;
            }
          clutter_actor_set_position (msg_icon, MSG_ICON_POS);
          clutter_actor_set_size (msg_icon, ICON_SIZE);
          clutter_actor_set_opacity (msg_icon, ICON_OPACITY);
          clutter_actor_add_child (CLUTTER_ACTOR (self), msg_icon);
        }
      g_free (hint_name);
      g_variant_unref (out_variant);
      break;
    }
  return TRUE;
}

static void
button_release (ClutterActor* actor, gpointer user_data)
{
  MildenhallNotificationWindow *self = user_data;
  ClutterContent *content = NULL;

  content = clutter_actor_get_content (self->released_img);
  clutter_actor_set_content (actor, content);
}

static void
button_pressed_cb (ClutterActor* actor, gpointer user_data)
{
  MildenhallNotificationWindow *self = user_data;
  ClutterContent* content;

  content = clutter_actor_get_content (self->pressed_img);
  clutter_actor_set_content (actor, content);
}

static void
button_release_cb (ClutterActor* actor, gpointer user_data)
{
  MildenhallNotificationWindow *self = user_data;
  const gchar *button_name = NULL;

  button_release (actor, user_data);
  button_name = clutter_actor_get_name (actor);
  g_signal_emit_by_name (self, "action-invoked", button_name);
  if (self->source_id > 0)
    {
      g_source_remove (self->source_id);
      self->source_id = 0;
    }
  mildenhall_notification_window_hide (self,
                                       MILDENHALL_NOTIFICATION_CLOSED_BY_USER);
}

static gboolean
leave_event_cb (ClutterActor* actor, ClutterEvent* pEvent, gpointer user_data)
{
  button_release (actor, user_data);
  return FALSE;
}

static void
swipe_action_cb (ClutterActor* actor, LightwoodSwipeDirection direction,
                 gpointer user_data)
{
  button_release (actor, user_data);
}

static void
mildenhall_notification_window_create_button (MildenhallNotificationWindow *self, const gchar *name,
                                              gfloat x, gfloat y,
                                              gfloat width, gfloat height)
{
  ClutterContent *content = NULL;
  MildenhallNotificationButton *button = NULL;

  content = clutter_actor_get_content (self->released_img);
  button = mildenhall_notification_button_new (width, height);
  clutter_actor_set_name (CLUTTER_ACTOR (button), name);
  clutter_actor_set_position (CLUTTER_ACTOR (button), x, y);
  clutter_actor_set_content (CLUTTER_ACTOR (button), content);
  clutter_actor_set_reactive (CLUTTER_ACTOR (button), TRUE);
  clutter_actor_add_child (CLUTTER_ACTOR (self), CLUTTER_ACTOR (button));
  g_signal_connect (button, "button-press", G_CALLBACK (button_pressed_cb),
                    self);
  g_signal_connect (button, "button-release", G_CALLBACK (button_release_cb),
                    self);
  g_signal_connect (button, "button-long-press", G_CALLBACK (button_release),
                    self);
  g_signal_connect (button, "leave-event", G_CALLBACK (leave_event_cb), self);
  g_signal_connect (button, "swipe-action", G_CALLBACK (swipe_action_cb), self);

}


static gboolean
mildenhall_notification_window_set_action_buttons (
    MildenhallNotificationWindow *self, const gchar * const *actions,
    GError **error)
{
  guint length;
  ClutterColor color_enable;
  ClutterColor color_disable;
  ClutterActor *button_text1 = NULL;
  ClutterActor *button_text2 = NULL;
  gfloat width = 0;
  gfloat height = 0;
  gint pos_x;
  gint pos_y;

  length = g_strv_length ((gchar **) actions);
  clutter_color_from_string (&color_enable, COLOUR_ENABLE);
  clutter_color_from_string (&color_disable, COLOUR_DISABLE);
  if (length == 2)
    {
      /*single button   */
      mildenhall_notification_window_create_button (self, actions[0],
                                                    SINGLE_BUTTON_POS,
                                                    SINGLE_BUTTON_SIZE);
      button_text1 = clutter_text_new_with_text (BUTTON_TEXT_FONT, actions[1]);
      clutter_text_set_color (CLUTTER_TEXT (button_text1), &color_enable);
      clutter_actor_get_size (button_text1, &width, &height);
      pos_x = (SINGLE_BUTTON_TEXT_XOFFSET - (width / 2));
      pos_y = SINGLE_BUTTON_TEXT_YOFFSET - height;
      clutter_actor_set_position (button_text1, pos_x, pos_y);
      clutter_actor_add_child (CLUTTER_ACTOR (self), button_text1);
    }
  else if (length == 4)
    {
      /*two button   */
      mildenhall_notification_window_create_button (self, actions[0],
                                                    DOUBLE_BUTTON_BUTTON1_POS,
                                                    DOUBLE_BUTTON_BUTTON1_SIZE);
      mildenhall_notification_window_create_button (self, actions[2],
                                                    DOUBLE_BUTTON_BUTTON2_POS,
                                                    DOUBLE_BUTTON_BUTTON2_SIZE);

      button_text1 = clutter_text_new_with_text (BUTTON_TEXT_FONT, actions[1]);
      clutter_text_set_color (CLUTTER_TEXT (button_text1), &color_enable);
      clutter_actor_get_size (button_text1, &width, &height);
      pos_x = (DOUBLE_BUTTON_BUTTON1_XOFFSET - (width / 2));
      pos_y = DOUBLE_BUTTON_BUTTON1_YOFFSET - height;
      clutter_actor_set_position (button_text1, pos_x, pos_y);
      clutter_actor_add_child (CLUTTER_ACTOR (self), button_text1);
      button_text2 = clutter_text_new_with_text (BUTTON_TEXT_FONT, actions[3]);
      clutter_text_set_color (CLUTTER_TEXT (button_text2), &color_disable);
      clutter_actor_get_size (button_text2, &width, &height);
      pos_x = (DOUBLE_BUTTON_BUTTON2_XOFFSET - (width / 2));
      pos_y = DOUBLE_BUTTON_BUTTON2_YOFFSET - height;
      clutter_actor_set_position (button_text2, pos_x, pos_y);
      clutter_actor_add_child (CLUTTER_ACTOR (self), button_text2);
    }
  else
    {
      g_set_error_literal (error, MILDENHALL_NOTIFICATION_WINDOW_ERROR,
                           MILDENHALL_NOTIFICATION_INVALID_NUMBER_OF_ACTIONS,
                           "Invalid no of actions");
      return FALSE;
    }

  return TRUE;
}

static gboolean
mildenhall_notification_window_init_failable (GInitable *initable,
                                              GCancellable *cancellable,
                                              GError **error)
{
  MildenhallNotificationWindow *self = (MildenhallNotificationWindow *) initable;
  ClutterColor textbgcolor;
  ClutterActor *text_bg = NULL;
  ClutterActor *head_bar = NULL;
  ClutterActor *left_border = NULL;
  ClutterActor *right_border = NULL;
  ClutterActor *between_bar = NULL;
  ClutterActor *app_icon = NULL;

  head_bar = create_texture_from_file (NOTIFICATIONSDIR "/bt_bg_head.png", HEAD_BAR_SIZE, error);
  if (error && *error)
    goto cleanup_if_fail;
  left_border = create_texture_from_file (NOTIFICATIONSDIR "/bt_bg_popup_seamless_inverted.png", LEFT_BORDER_SIZE, error);
  if (error && *error)
    goto cleanup_if_fail;
  right_border = create_texture_from_file (NOTIFICATIONSDIR "/bt_bg_popup_seamless_inverted.png", RIGHT_BORDER_SIZE, error);
  if (error && *error)
    goto cleanup_if_fail;
  between_bar = create_texture_from_file (NOTIFICATIONSDIR "/bt_bg_popup_between.png", BETWEEN_BAR_SIZE, error);
  if (error && *error)
    goto cleanup_if_fail;
  self->released_img = create_texture_from_file (NOTIFICATIONSDIR "/bt_bg_popup_seamless_normal.png", 0.0, BUTTON_HEIGHT, error);
  if (error && *error)
    goto cleanup_if_fail;
  self->pressed_img = create_texture_from_file (NOTIFICATIONSDIR "/bt_bg_popup_seamless_pressed.png", 0.0, BUTTON_HEIGHT, error);
  if (error && *error)
    goto cleanup_if_fail;

  clutter_actor_set_position (head_bar, HEAD_BAR_POSITION);
  clutter_actor_set_size (head_bar, HEAD_BAR_SIZE);
  clutter_actor_add_child (CLUTTER_ACTOR (self), head_bar);

  text_bg = clutter_actor_new ();
  clutter_color_from_string (&textbgcolor, TEXT_BG_COLOUR);
  clutter_actor_set_background_color (text_bg, &textbgcolor);
  clutter_actor_set_position (text_bg, TEXT_BG_POS);
  clutter_actor_set_size (text_bg, TEXT_BG_SIZE);
  clutter_actor_set_opacity (text_bg, TEXT_BG_OPACITY);
  clutter_actor_add_child (CLUTTER_ACTOR (self), text_bg);

  clutter_actor_set_position (left_border, LEFT_BORDER_POS);
  clutter_actor_set_size (left_border, LEFT_BORDER_SIZE);
  clutter_actor_set_opacity (left_border, LEFT_BORDER_OPACITY);
  clutter_actor_add_child (CLUTTER_ACTOR (self), left_border);

  clutter_actor_set_position (right_border, RIGHT_BORDER_POS);
  clutter_actor_set_size (right_border, RIGHT_BORDER_SIZE);
  clutter_actor_set_opacity (right_border, RIGHT_BORDER_OPACITY);
  clutter_actor_add_child (CLUTTER_ACTOR (self), right_border);

  clutter_actor_set_position (between_bar, BETWEEN_BAR_POS);
  clutter_actor_set_size (between_bar, BETWEEN_BAR_SIZE);
  clutter_actor_add_child (CLUTTER_ACTOR (self), between_bar);

  g_signal_connect (self, "transitions-completed",
                    G_CALLBACK (notification_transition_completed_cb), self);

  self->timeout = self->notification->expire_timeout;
  app_icon = create_texture_from_file (self->notification->app_icon,
                                       ICON_SIZE, NULL);
  clutter_actor_set_position (app_icon, APP_ICON_POS);
  clutter_actor_set_size (app_icon, ICON_SIZE);
  clutter_actor_set_opacity (app_icon, ICON_OPACITY);
  clutter_actor_add_child (CLUTTER_ACTOR (self), app_icon);
  mildenhall_notification_window_set_body_text (self,
                                                self->notification->body);
  if (!mildenhall_notification_window_set_action_buttons (
      self, (const gchar * const *) self->notification->actions, error))
    goto cleanup_if_fail;
  if (!mildenhall_notification_window_set_hints (self,
                                                 self->notification->hints,
                                                 error))
    goto cleanup_if_fail;

  clutter_actor_set_position (CLUTTER_ACTOR (self), WINDOW_POS);
  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);
  return TRUE;

cleanup_if_fail:
  g_clear_pointer (&text_bg, clutter_actor_destroy);
  g_clear_pointer (&head_bar, clutter_actor_destroy);
  g_clear_pointer (&left_border, clutter_actor_destroy);
  g_clear_pointer (&right_border, clutter_actor_destroy);
  g_clear_pointer (&between_bar, clutter_actor_destroy);
  g_clear_pointer (&app_icon, clutter_actor_destroy);
  g_clear_pointer (&self->released_img, clutter_actor_destroy);
  g_clear_pointer (&self->pressed_img, clutter_actor_destroy);

  return FALSE;
}

static void
mildenhall_notification_window_initable_init (GInitableIface *iface)
{
  iface->init = mildenhall_notification_window_init_failable;
}

static void
mildenhall_notification_window_dispose (GObject *object)
{
  MildenhallNotificationWindow *self = MILDENHALL_NOTIFICATION_WINDOW (object);

  g_clear_pointer (&self->notification, mildenhall_notification_free);
  G_OBJECT_CLASS (mildenhall_notification_window_parent_class)->dispose (
      object);
}

static void
mildenhall_notification_window_class_init (MildenhallNotificationWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = mildenhall_notification_window_get_property;
  object_class->set_property = mildenhall_notification_window_set_property;
  object_class->dispose = mildenhall_notification_window_dispose;

  properties[PROP_NOTIFICATION_STATE] = g_param_spec_enum (
      "state", "state", "state of the popup",
      mildenhall_notification_state_get_type (), MILDENHALL_NOTIFICATION_NONE,
      G_PARAM_READABLE);

  properties[PROP_NOTIFICATION_ID] = g_param_spec_uint ("id", "id",
                                                        "Id of the popup", 1,
                                                        G_MAXUINT, 1,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  properties[PROP_NOTIFICATION_STRUCT] = g_param_spec_boxed ("notification", "notification",
                                                             "notification structure containing the mildenhall notification information",
                                                             MILDENHALL_TYPE_NOTIFICATION,
                                                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, PROP_LAST, properties);

  notification_signals[SIG_NOTIFICATION_ACTION] = g_signal_new ("action-invoked",
                                                                G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST,
                                                                0, NULL, NULL,
                                                                g_cclosure_marshal_generic, G_TYPE_NONE,
                                                                1, G_TYPE_STRING);
}

static void
mildenhall_notification_window_init (MildenhallNotificationWindow *self)
{
  self->reason = 0;
  self->source_id = 0;
  self->pressed_img = NULL;
  self->released_img = NULL;
  self->state = MILDENHALL_NOTIFICATION_NONE;
}

MildenhallNotificationWindow *
mildenhall_notification_window_new (MildenhallNotification *notification,
                                    GError **error)
{

  MildenhallNotificationWindow *self = NULL;

  g_return_val_if_fail (notification != NULL, NULL);
  self = g_initable_new (MILDENHALL_TYPE_NOTIFICATION_WINDOW, NULL, error,
                         "id", notification->id,
                         "notification", notification,
                         NULL);
  return self;

}

void
mildenhall_notification_window_hide (MildenhallNotificationWindow *self,
                                     MildenhallNotificationClosedReason reason)
{
  g_return_if_fail (MILDENHALL_IS_NOTIFICATION_WINDOW (self));
  g_return_if_fail (self->state != MILDENHALL_NOTIFICATION_NONE);
  self->reason = reason;
  self->state = MILDENHALL_NOTIFICATION_HIDING;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NOTIFICATION_STATE]);
  clutter_actor_save_easing_state (CLUTTER_ACTOR (self));
  clutter_actor_set_easing_mode (CLUTTER_ACTOR (self), CLUTTER_EASE_OUT_BOUNCE);
  clutter_actor_set_easing_duration (CLUTTER_ACTOR (self), EASING_DURATION_MS);
  clutter_actor_set_y (CLUTTER_ACTOR (self), clutter_actor_get_y (CLUTTER_ACTOR (self))
          + clutter_actor_get_height (CLUTTER_ACTOR (self)));
  clutter_actor_restore_easing_state (CLUTTER_ACTOR (self));
}

static gboolean
expiry_timeout_cb (gpointer user_data)
{
  mildenhall_notification_window_hide (
      MILDENHALL_NOTIFICATION_WINDOW (user_data), MILDENHALL_NOTIFICATION_CLOSED_EXPIRED);
  return G_SOURCE_REMOVE;
}

void
mildenhall_notification_window_show (MildenhallNotificationWindow *self)
{
  g_return_if_fail (MILDENHALL_IS_NOTIFICATION_WINDOW (self));
  g_return_if_fail (self->state != MILDENHALL_NOTIFICATION_SHOWN);
  self->state = MILDENHALL_NOTIFICATION_SHOWING;
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_NOTIFICATION_STATE]);
  clutter_actor_save_easing_state (CLUTTER_ACTOR (self));
  clutter_actor_set_easing_mode (CLUTTER_ACTOR (self), CLUTTER_EASE_OUT_BOUNCE);
  clutter_actor_set_easing_duration (CLUTTER_ACTOR (self), EASING_DURATION_MS);
  clutter_actor_set_y (
      CLUTTER_ACTOR (self),
      clutter_actor_get_y (CLUTTER_ACTOR (self))
          - clutter_actor_get_height (CLUTTER_ACTOR (self)));
  clutter_actor_restore_easing_state (CLUTTER_ACTOR (self));
  if (self->timeout > 0)
    {
      self->source_id = g_timeout_add_seconds (self->timeout, expiry_timeout_cb,
                                               self);
    }
}

MildenhallNotificationClosedReason
mildenhall_notification_window_get_notification_closed_reason (
    MildenhallNotificationWindow *self)
{
  g_return_val_if_fail (MILDENHALL_IS_NOTIFICATION_WINDOW (self), 0);

  return self->reason;
}

guint
mildenhall_notification_window_get_id (MildenhallNotificationWindow *self)
{
  g_return_val_if_fail (MILDENHALL_IS_NOTIFICATION_WINDOW (self), 0);

  return self->notification_id;
}

MildenhallNotificationState
mildenhall_notification_window_get_state (MildenhallNotificationWindow *self)
{
  g_return_val_if_fail (MILDENHALL_IS_NOTIFICATION_WINDOW (self),
                        MILDENHALL_NOTIFICATION_NONE);

  return self->state;
}

GQuark
mildenhall_notification_window_error_quark (void)
{
  return g_quark_from_static_string ("mildenhall-notification-window-error");
}
