/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-idle-animation.h"
#include "mildenhall-mutter-plugin.h"

#define IDLE_ANIM_FADE_IN_TIMELINE	800
#define IDLE_ANIM_FADE_OUT_TIMELINE	1800
#define IDLE_ANIM_TIMELINE	6000

static gboolean bAnimationDataInitialized = FALSE;
guint idle_animation_event_source_id = 0;

#define ANIMATION_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), IDLE_TYPE_ANIMATION, IdleAnimationPrivate))

struct _IdleAnimationPrivate
{
	ClutterActor *spinner[3];
	ClutterTimeline *animTimeline;
	ClutterTimeline *fadeInTimeline;
	ClutterTimeline *fadeOutTimeline;
	gboolean running;
};

G_DEFINE_TYPE_WITH_PRIVATE (IdleAnimation, idle_animation, CLUTTER_TYPE_ACTOR)

/*********************************************************************************************
 * Function:    p_idle_animation_new
 * Description: Creates a new idle animation instance.
 * Parameters:
 * Return:      Newly created Idle Animation actor
 ********************************************************************************************/
ClutterActor *p_idle_animation_new(void)
{
	if (! bAnimationDataInitialized)
	{
        v_idle_animation_init_image_data ();
	}

	return g_object_new(IDLE_TYPE_ANIMATION, NULL);
}

/*********************************************************************************************
 * Function:    v_idle_animation_finalize
 * Description: Call a finalize on the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void v_idle_animation_finalize( GObject *pObject )
{
	G_OBJECT_CLASS( idle_animation_parent_class )->finalize( pObject );
}

/*********************************************************************************************
 * Function:    v_idle_animation_dispose
 * Description: Dispose the object
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void v_idle_animation_dispose( GObject *pObject )
{
	G_OBJECT_CLASS( idle_animation_parent_class )->dispose( pObject );
}

/*********************************************************************************************
 * Function:    idle_animation_class_init
 * Description: Class initialisation function for the object type.
 *				Called automatically on the first call to g_object_new
 * Parameters:  The object's class reference
 * Return:      void
 ********************************************************************************************/
static void idle_animation_class_init( IdleAnimationClass *pKlass )
{
	GObjectClass *pGobject_class = G_OBJECT_CLASS( pKlass );
	pGobject_class->finalize = v_idle_animation_finalize;
	pGobject_class->dispose  = v_idle_animation_dispose;
}

/*********************************************************************************************
 * Function:    g_timeline_callback
 * Description:
 * Parameters:  The ClutterTimeline with the no of frames
 * Return:      gboolean
 ********************************************************************************************/
static gboolean g_timeline_callback( ClutterTimeline *pTimeline, gint inframes, gpointer pUser_data )
{
	IdleAnimationPrivate *pPriv;
	guint inLoop = 0;
	gfloat gfAngularVel[3]   =  {+1.00, -1.00, -1.00};	// Angular velocity
	gfloat gfOffsets[3]	     =  {0.00, 15.00, 11.00};	// Angle Offsets
	gfloat gfRotation;

	g_return_val_if_fail( IDLE_IS_ANIMATION(pUser_data), FALSE );
	pPriv = IDLE_ANIMATION( pUser_data )->pPriv;
	gfRotation = (clutter_timeline_get_progress (pPriv->animTimeline) * 500.00f);

	if (inframes % 2)
	{
        return TRUE;
	}

	for(inLoop = 0; inLoop < 3; inLoop++)
	{
		clutter_actor_set_rotation_angle( pPriv->spinner[inLoop], CLUTTER_Z_AXIS, (gfOffsets[inLoop] + gfAngularVel[inLoop] * gfRotation));
	}
	return TRUE;
}


/*********************************************************************************************
 * Function:    idle_animation_init
 * Description: Instance initialisation function for the object type.
 *				Called automatically on every call to g_object_new
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static void idle_animation_init( IdleAnimation *pSelf )
{
	IdleAnimationPrivate *pPriv;
	guint inLoop = 0;
	GError *err = NULL;
	ClutterActor *pIdleAnimWheel = NULL;

	pSelf->pPriv = pPriv = ANIMATION_PRIVATE (pSelf);

	pIdleAnimWheel = p_ui_texture_from_file (PKGDATADIR"/idle_animation.png");
	  if (err)
	  {
	    g_warning ("unable to load texture: %s", err->message);
	    g_clear_error (&err);
	    return ;
	  }
	clutter_actor_set_opacity( pIdleAnimWheel, 0x00 );
	pPriv->spinner[0] = pIdleAnimWheel;
	clutter_actor_add_child( CLUTTER_ACTOR(pSelf), pPriv->spinner[0] );
	clutter_actor_set_position( pPriv->spinner[0], 0, 0 );

	for(inLoop = 1; inLoop < 3; inLoop++)
	{
		pPriv->spinner[inLoop] = clutter_clone_new(pPriv->spinner[0]);
		clutter_actor_hide(pPriv->spinner[inLoop]);
		clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pPriv->spinner[inLoop]);
	}

	clutter_actor_set_position(pPriv->spinner[1], -71.00, 0.00);
	clutter_actor_set_position(pPriv->spinner[2], 21.00, 68.00);
	for(inLoop = 0; inLoop < 3; inLoop++)
	{
		clutter_actor_set_pivot_point(pPriv->spinner[inLoop], 0.5 , 0.5);
	}
	// Timeline to Rotate the idle animation wheels.
	pPriv->animTimeline = clutter_timeline_new ( IDLE_ANIM_TIMELINE );
	clutter_timeline_set_repeat_count ( pPriv->animTimeline, -1 );

	g_signal_connect( pPriv->animTimeline, "new-frame", G_CALLBACK(g_timeline_callback), pSelf );
	clutter_timeline_set_repeat_count( pPriv->animTimeline, -1 );
	pPriv->running = FALSE;
}

/*********************************************************************************************
 * Function:    v_hide_animation_cb
 * Description: callback function for hide animation
 * Parameters:  The ClutterTimeline with the object reference
 * Return:      void
 ********************************************************************************************/
static void v_hide_animation_cb (ClutterActor *actor, gpointer pUser_data)
{
	IdleAnimationPrivate *pPriv = IDLE_ANIMATION ( pUser_data )->pPriv;
	guint inLoop = 0;
	g_signal_handler_disconnect(actor , idle_animation_event_source_id);
	for(inLoop = 0; inLoop < 3; inLoop++)
	{
		clutter_actor_hide ( CLUTTER_ACTOR ( pPriv->spinner[inLoop] ) );
	}
	if ( clutter_timeline_is_playing ( pPriv->animTimeline ) )
	{
        clutter_timeline_stop ( pPriv->animTimeline );
	}
}

/*********************************************************************************************
 * Function:    v_idle_animation_start
 * Description: Start the idle animation. Once the idle animation starts, it is an indication
 *              to the user that the system is busy doing some background processing work
 * Parameters:  An instance of the idle animation as returned by idle_animation_new()
 * Return:      void
 ********************************************************************************************/
void v_idle_animation_start( ClutterActor *pIdleanimation )
{
	IdleAnimationPrivate *pPriv = NULL;
	gint guiLoop;

	if ( NULL == clutter_actor_get_parent ( pIdleanimation ) )
	{
        return;
	}

	g_return_if_fail(IDLE_IS_ANIMATION(pIdleanimation));
	pPriv = IDLE_ANIMATION (pIdleanimation)->pPriv;

	if (pPriv->running)
	{
		return;
	}

	for(guiLoop = 0; guiLoop < 3; guiLoop++)
	{
		#pragma GCC diagnostic push
		#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
		clutter_actor_detach_animation (pPriv->spinner[guiLoop]);
		#pragma GCC diagnostic pop

    clutter_actor_set_opacity ( pPriv->spinner[guiLoop], 0 );
    clutter_actor_show ( pPriv->spinner[guiLoop] );
    clutter_actor_save_easing_state (pPriv->spinner[guiLoop]);
    clutter_actor_set_easing_mode (pPriv->spinner[guiLoop], CLUTTER_LINEAR);
    clutter_actor_set_easing_duration (pPriv->spinner[guiLoop], IDLE_ANIM_FADE_IN_TIMELINE);
    clutter_actor_set_opacity (pPriv->spinner[guiLoop], 0xff);
    clutter_actor_restore_easing_state (pPriv->spinner[guiLoop]);
	}
	if (! clutter_timeline_is_playing (pPriv->animTimeline))
	{
        clutter_timeline_start(pPriv->animTimeline);
	}
	pPriv->running = TRUE;
}


/*********************************************************************************************
 * Function:    v_idle_animation_stop
 * Description: Stop the idle animation.It is stopped when the application has completed any bg
 *              processing work,indicating to the user that the system is usable again
 * Parameters:  An instance of the idle animation as returned by idle_animation_new()
 * Return:      void
 ********************************************************************************************/
void v_idle_animation_stop( ClutterActor *pIdleanimation )
{
	IdleAnimationPrivate *pPriv = NULL;
	gint guiLoop;

	g_return_if_fail( IDLE_IS_ANIMATION(pIdleanimation) );
	pPriv = IDLE_ANIMATION ( pIdleanimation )->pPriv;

	if (!pPriv->running)
	{
        return;
	}
	for(guiLoop = 0; guiLoop < 3; guiLoop++)
	{
		#pragma GCC diagnostic push
		#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
	  clutter_actor_detach_animation (pPriv->spinner[guiLoop]);
		#pragma GCC diagnostic pop

		clutter_actor_set_opacity ( pPriv->spinner[guiLoop], 0xFF );
    clutter_actor_save_easing_state (pPriv->spinner[guiLoop]);
    clutter_actor_set_easing_mode (pPriv->spinner[guiLoop], CLUTTER_LINEAR);
    clutter_actor_set_easing_duration (pPriv->spinner[guiLoop], IDLE_ANIM_FADE_OUT_TIMELINE);
    clutter_actor_set_opacity (pPriv->spinner[guiLoop], 0);
    clutter_actor_restore_easing_state (pPriv->spinner[guiLoop]);
	}

	idle_animation_event_source_id =  g_signal_connect (pPriv->spinner[0],
                                    "transitions-completed",
                                    G_CALLBACK (v_hide_animation_cb),
                                    pIdleanimation);
	pPriv->running = FALSE;
}


/*********************************************************************************************
 * Function:    v_idle_animation_init_image_data
 * Description: Initialise the image data for the idle animation. The idle animation has a set
 *               of wheels rolling together (The images to create this widget are loaded here)
 * Parameters:
 * Return:      void
 ********************************************************************************************/
void v_idle_animation_init_image_data (void)
{
	bAnimationDataInitialized = TRUE;
}


/*********************************************************************************************
 * Function:    v_idle_animation_resume
 * Description: Resume the idle animation that was previously stared
 * Parameters:  An instance of the idle animation as returned by idle_animation_new()
 * Return:      void
 ********************************************************************************************/
void v_idle_animation_resume (ClutterActor *pIdleAnimation)
{
	IdleAnimationPrivate *pPriv = NULL;

	if ( NULL == pIdleAnimation )
	{
        return;
	}
	pPriv = IDLE_ANIMATION (pIdleAnimation)->pPriv;

	if (!clutter_timeline_is_playing ( pPriv->animTimeline ))
	{
		clutter_actor_show ( pIdleAnimation );
		clutter_timeline_set_repeat_count ( pPriv->animTimeline, -1 );
		clutter_timeline_start ( pPriv->animTimeline );
	}
}

/*********************************************************************************************
 * Function:    v_idle_animation_pause
 * Description: Pause an idle animation that is running
 * Parameters:  An instance of the idle animation as returned by idle_animation_new()
 * Return:      void
 ********************************************************************************************/
void v_idle_animation_pause (ClutterActor *pIdleAnimation)
{
	IdleAnimationPrivate *pPriv = NULL;

	if ( NULL == pIdleAnimation )
	{
        return;
	}
	pPriv = IDLE_ANIMATION (pIdleAnimation)->pPriv;

	if (clutter_timeline_is_playing (pPriv->animTimeline))
	{
		clutter_actor_hide ( pIdleAnimation );
		clutter_timeline_pause ( pPriv->animTimeline );
	}
}
