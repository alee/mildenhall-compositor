/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <meta/main.h>

#include "mildenhall-mutter-plugin.h"

gint
main (gint argc, gchar **argv)
{
  GOptionContext *context = NULL;
  GError *error = NULL;
  gint exit_status = 0;

  context = meta_get_option_context ();
  if (!g_option_context_parse (context, &argc, &argv, &error))
  {
    g_printerr ("Commandline options parsing failed: %s\n", error->message);
    g_error_free (error);
    exit_status = 1;
    goto out;
  }

  meta_plugin_manager_set_plugin_type (MILDENHALL_TYPE_PLUGIN);
  meta_init ();

  exit_status = meta_run ();
  DEBUG ("Mutter exit status = %d", exit_status);

out:
  return exit_status;
}
