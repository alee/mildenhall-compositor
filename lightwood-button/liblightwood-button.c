/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* button.c */
/**
 * SECTION:liblightwood-button
 * @Title:LightwoodButton
 * @Short_Description: Button Abstract class.
 *
 * #LightwoodButton will provide all generic properties required to create a button widget.
 * As #LightwoodButton is abstract class, it has to be inherited to have more properties and signals 
 * for the variant specification.In Lightwood For all button type widgets will have #LightwoodButton as base class.
 * It can be customized as required.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 * G_DEFINE_TYPE (TestButton, test_button, LIGHTWOOD_TYPE_BUTTON)
 *
 * #define BUTTON_PRIVATE(o) \
 *   (G_TYPE_INSTANCE_GET_PRIVATE ((o), TEST_TYPE_BUTTON, TestButtonPrivate)
 * 
 *	
 *static void test_long_press(GObject *pObject, gpointer pUserData)
 *{
 *        TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);
 *}
 *
 *static void test_button_swiped(GObject *pObject, LightwoodSwipeDirection inDirection, gpointer pUserData)
 *{
 *        TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);
 *
 *	TestButton *pButton = TEST_BUTTON(pObject);
 *	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(CLUTTER_ACTOR(pButton)));
 *	switch(inDirection)
 *	{
 *		case LIGHTWOOD_SWIPE_DIRECTION_LEFT:
 *			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_LEFT:\n");
 *			break;
 *		case LIGHTWOOD_SWIPE_DIRECTION_RIGHT:
 *			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_RIGHT:\n");
 *			break;
 *		case LIGHTWOOD_SWIPE_DIRECTION_UP:
 *			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_UP:\n");
 *			break;
 *		case LIGHTWOOD_SWIPE_DIRECTION_DOWN:
 *			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = LIGHTWOOD_SWIPE_DIRECTION_DOWN:\n");
 *			break;	
 *		default:
 *			TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: direction = UNKNOWN\n");	
 *	}
 *	test_button_released(pObject, NULL);	
 *}
 *
 *static void test_button_pressed(GObject *pObject, gpointer pUserData)
 *{
 *	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);
 *
 *	TestButton *pButton = TEST_BUTTON(pObject);
 *	TestButtonPrivate *priv = BUTTON_PRIVATE (pButton);
 *
 *	ClutterColor color = {0x00, 0xff, 0xff, 0xaa};
 *	if(NULL != priv->pRect)
 *	{
 *		g_object_set(priv->pRect, "background-color", &color, NULL);
 *	}
 *	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(CLUTTER_ACTOR(pButton)));
 *} 
 *
 *static void test_button_released(GObject *pObject, gpointer pUserData)
 *{
 *	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", __FUNCTION__);
 *
 *	TestButton *pButton = TEST_BUTTON(pObject);
 *	TestButtonPrivate *priv = BUTTON_PRIVATE (pButton);
 *
 *	ClutterColor white = {0xff, 0xff, 0xff, 0xff};
 *
 *        if(NULL != priv->pRect)
 *        {
 *                g_object_set(priv->pRect, "background-color", &white, NULL);
 *        }
 *
 *	TEST_BUTTON_DEGUG("TEST_BUTTON_DEBUG: %s\n", clutter_actor_get_name(CLUTTER_ACTOR(pButton)));
 *} 
 *
 *static void test_button_get_property (GObject    *object,
 *                          guint       property_id,
 *                          GValue     *value,
 *                          GParamSpec *pspec)
 *{
 *  switch (property_id)
 *    {
 *    default:
 *      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
 *    }
 *}
 *
 *static void test_button_set_property (GObject      *object,
 *                          guint         property_id,
 *                          const GValue *value,
 *                          GParamSpec   *pspec)
 *{
 *  switch (property_id)
 *   {
 *   default:
 *     G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
 *   }
 *}
 *
 *static void test_button_dispose (GObject *object)
 *{
 *  G_OBJECT_CLASS (test_button_parent_class)->dispose (object);
 *}
 *
 *static void test_button_finalize (GObject *object)
 *{
 *  G_OBJECT_CLASS (test_button_parent_class)->finalize (object);
 *}
 *
 *static void test_button_class_init (TestButtonClass *klass)
 *{
 *  GObjectClass *object_class = G_OBJECT_CLASS (klass);
 *
 *  g_type_class_add_private (klass, sizeof (TestButtonPrivate));
 *
 *  object_class->get_property = test_button_get_property;
 *  object_class->set_property = test_button_set_property;
 *  object_class->dispose = test_button_dispose;
 *  object_class->finalize = test_button_finalize;
 *}
 *
 *static void test_button_init (TestButton *self)
 *{
 *	self->priv = BUTTON_PRIVATE (self);
 *	g_object_set(self, "width", 64.0, "height", 64.0, NULL);
 *		
 *	ClutterColor white = {0xff, 0xff, 0xff, 0xff};
 *
 *	//self->priv->pRect = clutter_rectangle_new_with_color(&white);
 *	self->priv->pRect = clutter_actor_new();
 *	g_object_set(self->priv->pRect, "background-color", &white, NULL);
 *	clutter_actor_set_size(self->priv->pRect, 64, 64);
 *	
 *	clutter_actor_add_child(CLUTTER_ACTOR(self), self->priv->pRect);
 *
 *	// lightwood button signals 
 *	g_signal_connect(self, "button-press", G_CALLBACK(test_button_pressed), NULL);
 *        g_signal_connect(self, "button-release", G_CALLBACK(test_button_released), NULL);
 *        g_signal_connect(self, "swipe-action", G_CALLBACK(test_button_swiped), NULL);
 *        g_signal_connect(self, "button-long-press", G_CALLBACK(test_long_press), NULL);
 *        
 *	// leave signal 
 *	//g_signal_connect(self, "leave-event", G_CALLBACK(test_button_leave), NULL);
 *
 *}
 *
 *ClutterActor *test_button_new (void)
 *{
 *  return g_object_new (TEST_TYPE_BUTTON, NULL);
 *}
 *
 *int main (int argc, char **argv)
 *{
 *	int clInErr = clutter_init(&argc, &argv);
 *        if (clInErr != CLUTTER_INIT_SUCCESS)
 *                return -1;
 *
 *	//clutter_threads_init ();
 *
 *
 *	gfloat width, height;
 *	gboolean bLongPress, bSwipe;
 *	gchar *name = NULL;
 *
 *        ClutterActor *pStage;
 *        ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };
 *
 *        pStage = clutter_stage_new ();
 *        clutter_actor_set_background_color (pStage, &black);
 *        clutter_actor_set_size (pStage, 728, 480);
 *	g_signal_connect (pStage, "destroy", G_CALLBACK (clutter_main_quit), NULL);
 *
 *	ClutterActor *pButton = test_button_new();
 *	g_object_set(pButton, "swipe-enabled", TRUE, "reactive", TRUE, "long-press-enabled", TRUE, "name", "Button1", NULL);
 *
 *	clutter_actor_add_child(pStage, pButton);
 *
 *	ClutterActor *pButton2 = test_button_new();
 *        g_object_set(pButton2, "swipe-enabled", FALSE, "long-press-enabled", FALSE, "reactive", TRUE, "name", "Button2", NULL);
 *	clutter_actor_set_position(pButton2, 65, 0);
 *
 *	clutter_actor_add_child(pStage, pButton2);
 *
 *	g_object_get(pButton2, "name", &name, "width", &width, "height", &height, "long-press-enabled", &bLongPress, "swipe-enabled", &bSwipe, NULL);
 *	TEST_BUTTON_DEGUG("Button2 :\n name = %s width = %f height = %f long-press-enabled = %d swipe-enabled = %d\n", name, width, height, bLongPress, bSwipe);
 *
 *	g_object_get(pButton, "name", &name, "width", &width, "height", &height, "long-press-enabled", &bLongPress, "swipe-enabled", &bSwipe, NULL);
 *	TEST_BUTTON_DEGUG("Button1 : \n name = %s width = %f height = %f long-press-enabled = %d swipe-enabled = %d\n", name, width, height, bLongPress, bSwipe);
 *
 *        clutter_actor_show (pStage);
 *
 *        clutter_main();
 *
 *	return 0;
 *}	
 *
 *	 
 * ]|
 *
 *
 * #LightwoodButton is available since lightwood-base 1.0
 */



#include "liblightwood-button.h"


typedef enum _enButtonProperty enButtonProperty;
typedef struct _LightwoodButtonPrivate LightwoodButtonPrivate;
typedef enum _enButtonSignals enButtonSignals;
typedef enum _enButtonState enButtonState;
typedef enum _enButtonDebugFlag enButtonDebugFlag;

/* Set the environment variable in terminal to enable traces: export LIGHTWOOD_BUTTON_DEBUG=lightwood-button */
enum _enButtonDebugFlag 
{
	LIGHTWOOD_BUTTON_DEBUG = 1 << 0,

};

/* button property enums */ 
enum _enButtonProperty
{
	/*<private>*/
        PROP_BUTTON_FIRST,
	PROP_BUTTON_SWIPE_ENABLED,
	PROP_BUTTON_REACTIVE,
	PROP_BUTTON_LONG_PRESS_ENABLED,
	PROP_BUTTON_TOOLTIP_DIRECTION,
	PROP_BUTTON_TOOLTIP_ENABLED,
	PROP_BUTTON_RESET_STATE,
	PROP_BUTTON_TOOLTIP_DURATION
};

/* The signals emitted by the lightwood button */
enum _enButtonSignals
{
	/*<private>*/
        SIG_BUTTON_FIRST_SIGNAL,
        SIG_BUTTON_PRESS,                       /* Emitted when user pressed the button */
        SIG_BUTTON_RELEASE,            		/* Emitted when button is released */
        SIG_BUTTON_LONG_PRESS,           	/* Emitted on long press */
	SIG_BUTTON_SWIPE_ACTION,			/* Emitted on swipe action */
	SIG_BUTTON_TOOLTIP_HIDDEN,
	SIG_BUTTON_TOOLTIP_SHOWN,
	SIG_BUTTON_RESET,
        SIG_BUTTON_LAST_SIGNAL
};

/* ecoe button states*/
enum _enButtonState
{
	/*<private>*/
	BUTTON_STATE_PRESSED,
	BUTTON_STATE_RELEASED,
	BUTTON_STATE_SWIPED,
	BUTTON_STATE_LONGPRESSED,
	BUTTON_STATE_UNKNOWN
};

/* swipe direction as GType */
//GType static button_swipe_direction_get_type (void) G_GNUC_CONST;
//#define BUTTON_TYPE_SWIPE_DIRECTION (button_swipe_direction_get_type()) 

/* get tooltip direction as GType */
static GType button_tooltip_direction_get_type (void) G_GNUC_CONST;
#define BUTTON_TYPE_TOOLTIP_DIRECTION (button_tooltip_direction_get_type()) 

#define DEFAULT_LONG_PRESS_DURATION	1200	/* should be defined in lightwood settings */
#define DEFAULT_RESET_DURATION          DEFAULT_LONG_PRESS_DURATION + 200    /* should be defined in lightwood settings */

/* private structure for the lightwood button */
struct _LightwoodButtonPrivate
{
	gboolean bSwipeEnable;
	gboolean bReactive;
	gboolean bLongPressEnable;
	gboolean bResetState;
	enButtonState enButtonState;

	LightwoodSwipeDirection hDirection;
  	LightwoodSwipeDirection vDirection;

	gint  inTooltipDuration;       // Duration for the tooltip show
        LightwoodTooltipDirection enDirection;         // tooltip direction
	gboolean bTooltipEnabled;       // Whether tooltip is enabled

  	gint inThreshold;	/* The default distance that the cursor of a pointer device should travel 
				 * before a swipe/drag operation should start.  */

	gint inResetTimeout;
	gint inPressTimeoutTimer;

	ClutterAction *pGestureAction;
	ClutterAction *pLongPressAction;

	ClutterLongPressState enLongPressState;
};

/* debug flag settings */
guint  button_debug_flags = 0;

static const GDebugKey button_debug_keys[] = 
{
	{ "lightwood-button",   LIGHTWOOD_BUTTON_DEBUG }
};

/* Storage for the signals */
static guint32 button_signals[SIG_BUTTON_LAST_SIGNAL] = {0,};

G_DEFINE_TYPE_WITH_PRIVATE (LightwoodButton, lightwood_button, CLUTTER_TYPE_ACTOR)

static gboolean button_release_cb(ClutterActor *pButton, ClutterButtonEvent *pEvent, gpointer pUserData);

#define BUTTON_HAS_DEBUG               ((button_debug_flags ) & 1)
#define LIGHTWOOD_BUTTON_PRINT( a ...) \
 	if (G_LIKELY (BUTTON_HAS_DEBUG )) \
	{                            	\
	       	g_print(a);           \
    	}


/********************************************************************************************************************
 *
 * Internal functions 
 *
 ********************************************************************************************************************/

/********************************************************
 * Function :   v_handle_gesture_end
 * Description: handling gesture end 
 * Parameters: pButton object, LightwoodSwipeDirection
 * Return value: 
 ********************************************************/
static void v_handle_gesture_end(LightwoodButton *pButton, LightwoodSwipeDirection enDirection)
{
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (pButton);
	/* if button is reactive and swipe is enabled, emit swipe-acton signal and update the button state */
        if(priv->bReactive && priv->bSwipeEnable && (priv->bResetState == FALSE) )
        {
                //if(priv->bResetState == FALSE)
                {
                        g_signal_emit (pButton, button_signals[SIG_BUTTON_SWIPE_ACTION], 0, enDirection);

                        priv->enButtonState = BUTTON_STATE_SWIPED;
                }
        }
        /* if swipe is not enabled, and current button state is pressed, emit the release event */
        else if(priv->bReactive
                        && !priv->bSwipeEnable
                        && priv->enButtonState == BUTTON_STATE_PRESSED && (priv->bResetState == FALSE))
        {
                //if(priv->bResetState == FALSE)
                {
                        // if swipe is disabled and button is pressed, release event should get emitted
                        button_release_cb(CLUTTER_ACTOR(pButton), NULL, NULL);
                }
        }
        /* if long press is disabled and long press state is Activated,
           check the current state of button, if it is pressed, emit the release event */
        else if(priv->bReactive && !priv->bLongPressEnable
                        && !priv->bSwipeEnable
                        && priv->enButtonState == BUTTON_STATE_PRESSED
                        && priv->enLongPressState == CLUTTER_LONG_PRESS_ACTIVATE
                        && (priv->bResetState == FALSE))
        {

                //if(priv->bResetState == FALSE)
                {
                        button_release_cb(CLUTTER_ACTOR(pButton), NULL, NULL);
                }
        }
}

/********************************************************
 * Function : 	en_get_left_right_direction
 * Description: returns left/right swipe direction
 * Parameters: gfloat, gfloat
 * Return value: LightwoodSwipeDirection
 ********************************************************/
static LightwoodSwipeDirection en_get_left_right_direction(gfloat flPressX, gfloat flReleaseX)
{
	LightwoodSwipeDirection enDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;

	/* if pressed Y coord is greater than the released one, direction will be up 
	 * otherwise down */
	if(flPressX >= flReleaseX)
	{
		LIGHTWOOD_BUTTON_PRINT("direction is left\n");
		enDirection = LIGHTWOOD_SWIPE_DIRECTION_LEFT;
	}
	else
	{
		LIGHTWOOD_BUTTON_PRINT("direction is right\n");
		enDirection = LIGHTWOOD_SWIPE_DIRECTION_RIGHT;
	}
	return enDirection;
}

/********************************************************
 * Function : 	en_get_direction
 * Description: returns swipe direction
 * Parameters: gfloat, gfloat
 * Return value: LightwoodSwipeDirection
 ********************************************************/
static LightwoodSwipeDirection en_get_direction(gfloat flPressY, gfloat flReleaseY, gfloat flPressX, gfloat flReleaseX, gfloat angle)
{
	LightwoodSwipeDirection enDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;
	if( (angle <= -45 && angle >= -90) || (angle >= 45 && angle <= 90) )
	{
		/* if pressed Y coord is greater than the released one, direction will be up 
		 * otherwise down */
		if(flPressY >= flReleaseY)
		{
			LIGHTWOOD_BUTTON_PRINT("direction is up\n");
			enDirection = LIGHTWOOD_SWIPE_DIRECTION_UP;
		}
		else
		{
			LIGHTWOOD_BUTTON_PRINT("direction is down\n");
			enDirection = LIGHTWOOD_SWIPE_DIRECTION_DOWN;
		}
	}
	else
	{
		enDirection = en_get_left_right_direction(flPressX, flReleaseX);
	}
	return enDirection;
}

/********************************************************
 * Function : b_button_reset_button
 * Description: timeout to switch back to button release 
 * Parameters: LightwoodButton*
 * Return value: gboolean
 ********************************************************/
static gboolean b_button_reset_button(gpointer pUserData)
{
	LightwoodButton *pButton;
	LightwoodButtonPrivate *priv;

	g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pUserData), FALSE);

	pButton = LIGHTWOOD_BUTTON(pUserData);
	priv = lightwood_button_get_instance_private (pButton);
        if(NULL != priv)
        {
        	if ( ! priv->bResetState)
       		{ 
	        	priv->bResetState = TRUE;
			/* emit the button-reset signal */ 
			g_signal_emit(pButton, button_signals[SIG_BUTTON_RESET], 0, NULL);
        	}
	}
        return FALSE;

}

/********************************************************
 * Function : v_button_stop_reset_timeout
 * Description: stop the reset timeout 
 * Parameters: LightwoodButton*
 * Return value: void
 ********************************************************/
static void v_button_stop_reset_timeout(LightwoodButton *pButton)
{
	LightwoodButtonPrivate *priv;

        g_return_if_fail (LIGHTWOOD_IS_BUTTON (pButton));

	priv = lightwood_button_get_instance_private (pButton);

	/* flag to reset the button if not released within reset duration */
        priv->bResetState = FALSE;

        // stop timeout to reset button
        if (priv->inPressTimeoutTimer > 0)
	{
		g_source_remove (priv->inPressTimeoutTimer);
		priv->inPressTimeoutTimer = 0;
	}

}


/********************************************************
 * Function : v_button_start_reset_timeout
 * Description: start the reset timeout  
 * Parameters: LightwoodButton*
 * Return value: void
 ********************************************************/
static void v_button_start_reset_timeout(LightwoodButton *pButton)
{
	LightwoodButtonPrivate *priv;

        g_return_if_fail (LIGHTWOOD_IS_BUTTON (pButton));

	priv = lightwood_button_get_instance_private (pButton);

	v_button_stop_reset_timeout(pButton);

	priv->inPressTimeoutTimer = g_timeout_add (priv->inResetTimeout, b_button_reset_button, pButton);
}

/********************************************************
 * Function : lightwood_button_swipe_direction_get_type
 * Description: define swipe direction enum as GType 
 * Parameters: void
 * Return value: void
 ********************************************************/
GType lightwood_button_swipe_direction_get_type (void)
{
	/* argument must point to a static 0-initialized variable,
	 * that will be set to a value other than 0 at the end of 
	 * the initialization section.
	 */
	static volatile gsize gEnumTypeVolatile = 0;

	/* swipe direction as enum initialization section */
	if (g_once_init_enter (&gEnumTypeVolatile))
	{ 
		/* A structure which contains a single flags value, its name, and its nickname. */
		static const GEnumValue arValues[] = 
		{
			{ LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN, "LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN", "unknown" },
			{ LIGHTWOOD_SWIPE_DIRECTION_UP, "LIGHTWOOD_SWIPE_DIRECTION_UP", "up" },
			{ LIGHTWOOD_SWIPE_DIRECTION_DOWN, "LIGHTWOOD_SWIPE_DIRECTION_DOWN", "down" },
			{ LIGHTWOOD_SWIPE_DIRECTION_LEFT, "LIGHTWOOD_SWIPE_DIRECTION_LEFT", "left" },
			{ LIGHTWOOD_SWIPE_DIRECTION_RIGHT, "LIGHTWOOD_SWIPE_DIRECTION_RIGHT", "right" },
			{ 0, NULL, NULL }
		};
		GType gEnumType;

		/* Registers a new static flags type with the name name. */
		gEnumType = g_enum_register_static (g_intern_static_string ("LightwoodSwipeDirection"), arValues);

		/* In combination with g_once_init_leave() and the unique address value_location, 
		 * it can be ensured that an initialization section will be executed only once 
		 * during a program's life time, and that concurrent threads are blocked 
		 * until initialization completed. 
		 */
		g_once_init_leave (&gEnumTypeVolatile, gEnumType);
	}

	return gEnumTypeVolatile;
}

/********************************************************
 * Function : button_tooltip_direction_get_type
 * Description: define tooltip direction enum as GType 
 * Parameters: void
 * Return value: void
 ********************************************************/
static GType button_tooltip_direction_get_type (void)
{
        /* argument must point to a static 0-initialized variable,
         * that will be set to a value other than 0 at the end of 
         * the initialization section.
         */
        static volatile gsize gEnumTypeVolatile = 0;

        /* swipe direction as enum initialization section */
        if (g_once_init_enter (&gEnumTypeVolatile))
        {
                /* A structure which contains a single flags value, its name, and its nickname. */
                static const GEnumValue arValues[] =
                {
                        { LIGHTWOOD_TOOLTIP_LEFT, "TOOLTIP_LEFT", "left" },
                        { LIGHTWOOD_TOOLTIP_RIGHT, "TOOLTIP_RIGHT", "right" },
                        { LIGHTWOOD_TOOLTIP_UP, "TOOLTIP_UP", "up" },
                        { LIGHTWOOD_TOOLTIP_DOWN, "TOOLTIP_DOWN", "down" },
                        { 0, NULL, NULL }
                };
                GType gEnumType;

                /* Registers a new static flags type with the name name. */
                gEnumType = g_enum_register_static (g_intern_static_string ("LightwoodTooltipDirection"), arValues);

                /* In combination with g_once_init_leave() and the unique address value_location, 
                 * it can be ensured that an initialization section will be executed only once 
                 * during a program's life time, and that concurrent threads are blocked 
                 * until initialization completed. 
                 */
                g_once_init_leave (&gEnumTypeVolatile, gEnumType);
        }

        return gEnumTypeVolatile;
}

/********************************************************
 * Function : lightwood_button_set_property 
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *	       for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void lightwood_button_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
	LightwoodButton *self = LIGHTWOOD_BUTTON (pObject);

	switch (uinPropertyID)
	{
		case PROP_BUTTON_SWIPE_ENABLED:
			lightwood_button_set_swipe_enabled (self, g_value_get_boolean (pValue));
			break;
		case PROP_BUTTON_LONG_PRESS_ENABLED:
			lightwood_button_set_long_press_enabled (self, g_value_get_boolean (pValue));
			break;	
		case PROP_BUTTON_REACTIVE:
			lightwood_button_set_reactive (self, g_value_get_boolean (pValue));
			break;
		case PROP_BUTTON_TOOLTIP_ENABLED:
			lightwood_button_set_tooltip_enabled (self, g_value_get_boolean (pValue));
                        break;
		case PROP_BUTTON_RESET_STATE:
			lightwood_button_set_button_reset_duration (self, g_value_get_int (pValue));
                        break;
                case PROP_BUTTON_TOOLTIP_DURATION:
			lightwood_button_set_tooltip_duration (self, g_value_get_int (pValue));
                        break;
                case PROP_BUTTON_TOOLTIP_DIRECTION:
			lightwood_button_set_tooltip_direction (self, g_value_get_enum (pValue));
                        break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : lightwood_button_get_property
 * Description: Get a property value
 * Parameters:  The object reference, property Id, 
 *	       	return location for where the property 
 *		value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void lightwood_button_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
	LightwoodButton *button = LIGHTWOOD_BUTTON (pObject);
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (button);

	switch (uinPropertyID)
	{
                case PROP_BUTTON_SWIPE_ENABLED:
			g_value_set_boolean (pValue, priv->bSwipeEnable);
			break;
                case PROP_BUTTON_LONG_PRESS_ENABLED:
			g_value_set_boolean (pValue, priv->bLongPressEnable);
			break;
                case PROP_BUTTON_REACTIVE:
			g_value_set_boolean (pValue, priv->bReactive);
			break;
		case PROP_BUTTON_TOOLTIP_ENABLED:
			g_value_set_boolean (pValue, priv->bTooltipEnabled);
                        break;
		case PROP_BUTTON_RESET_STATE:
			g_value_set_int (pValue, priv->inResetTimeout);
                        break;
                case PROP_BUTTON_TOOLTIP_DURATION:
			g_value_set_int (pValue, priv->inTooltipDuration);
                        break;
                case PROP_BUTTON_TOOLTIP_DIRECTION:
			g_value_set_enum (pValue, priv->enDirection);
                        break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/**
 * lightwood_button_set_reactive:
 * @self: a #LightwoodButton
 * @state: %TRUE to set @self reactive
 *
 * Update the #LightwoodButton:reactive property
 */
void
lightwood_button_set_reactive (LightwoodButton *self,
                               gboolean state)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  if (state == priv->bReactive)
    return;

  priv->bReactive = state;
  clutter_actor_set_reactive (CLUTTER_ACTOR (self), priv->bReactive);
  g_object_notify (G_OBJECT (self), "reactive");
}

/********************************************************
 * Function : lightwood_button_dispose
 * Description: Dispose the lightwood button object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void lightwood_button_dispose (GObject *pObject)
{
	/* TBD: free all memory held */

	G_OBJECT_CLASS (lightwood_button_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : lightwood_button_finalize
 * Description: Finalize the lightwood button object 
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void lightwood_button_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (lightwood_button_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : lightwood_button_press_cb 
 * Description: callback on lightwood button press
 * Parameters: The object reference, event pointer, userdata 
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean button_press_cb(ClutterActor *pButton, ClutterButtonEvent *pEvent, gpointer pUserData)
{
	LightwoodButtonPrivate *priv;

        g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pButton), FALSE);

	priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));

	if(priv->bReactive)
	{
		g_signal_emit(pButton, button_signals[SIG_BUTTON_PRESS], 0, NULL);		
		priv->enButtonState = BUTTON_STATE_PRESSED;

		v_button_start_reset_timeout(LIGHTWOOD_BUTTON(pButton) );

	}

	return FALSE;
}

/********************************************************
 * Function : button_release_cb 
 * Description: callback on lightwood button release
 * Parameters:  The button object reference, 
		ClutterButtonEvent*, userData
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean button_release_cb(ClutterActor *pButton, ClutterButtonEvent *pEvent, gpointer pUserData)
{
        LightwoodButtonPrivate *priv;

        g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pButton), FALSE);

        priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));

	v_button_stop_reset_timeout(LIGHTWOOD_BUTTON(pButton));

	/* check if button state is not released state, then only emit the release signat */
	if(priv->enButtonState != BUTTON_STATE_RELEASED)
	{
		if( (priv->bReactive && !priv->bSwipeEnable) || (priv->bReactive && priv->bSwipeEnable && priv->enButtonState != BUTTON_STATE_SWIPED))
		{
			if(priv->bResetState == FALSE)
				g_signal_emit(pButton, button_signals[SIG_BUTTON_RELEASE], 0, NULL);
		}

		priv->enButtonState = BUTTON_STATE_RELEASED;
	}
        return FALSE;
}

/********************************************************
 * Function : event_cb 
 * Description: callback on lightwood button press/release and touch
 * Parameters:  The button object reference, 
		ClutterButtonEvent*, userData
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean event_cb(ClutterActor *pButton, ClutterButtonEvent *pEvent, gpointer pUserData)
{
	switch (pEvent->type)
	{
		case CLUTTER_BUTTON_PRESS:
		case CLUTTER_TOUCH_BEGIN:
			button_press_cb(pButton, pEvent, pUserData);
		break;

		case CLUTTER_BUTTON_RELEASE:
		case CLUTTER_TOUCH_END:
			button_release_cb(pButton, pEvent, pUserData);			
		break;

		case CLUTTER_NOTHING:
		case CLUTTER_KEY_PRESS:
		case CLUTTER_KEY_RELEASE:
		case CLUTTER_MOTION:
		case CLUTTER_ENTER:
		case CLUTTER_LEAVE:
		case CLUTTER_SCROLL:
		case CLUTTER_STAGE_STATE:
		case CLUTTER_DESTROY_NOTIFY:
		case CLUTTER_CLIENT_MESSAGE:
		case CLUTTER_DELETE:
		case CLUTTER_TOUCH_UPDATE:
		case CLUTTER_TOUCH_CANCEL:
		case CLUTTER_EVENT_LAST:
		case CLUTTER_TOUCHPAD_PINCH:
		case CLUTTER_TOUCHPAD_SWIPE:
		case CLUTTER_PROXIMITY_IN:
		case CLUTTER_PROXIMITY_OUT:
		case CLUTTER_PAD_BUTTON_PRESS:
		case CLUTTER_PAD_BUTTON_RELEASE:
		case CLUTTER_PAD_STRIP:
		case CLUTTER_PAD_RING:
		default:
			return FALSE;
	}
	return FALSE;
}

/********************************************************
 * Function : button_gesture_begin
 * Description: callback on gesture begin on the the lightwood button
 * Parameters: ClutterGestureAction *, 
		button object reference, userData
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean button_gesture_begin(ClutterGestureAction *pAction, ClutterActor *pButton, gpointer pUserData)
{
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));
	ClutterSettings *settings = clutter_settings_get_default ();

        g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pButton), FALSE);


	v_button_start_reset_timeout(LIGHTWOOD_BUTTON(pButton));

	/* reset the state at the beginning of a new gesture */
	priv->hDirection = 0;
	priv->vDirection = 0;

	g_object_get (settings, "dnd-drag-threshold", &priv->inThreshold, NULL);

	LIGHTWOOD_BUTTON_PRINT("LIGHTWOOD_BUTTON_DEBUG: threshold = %d\n", priv->inThreshold);
	return TRUE;
}

/********************************************************
 * Function : button_gesture_progress
 * Description: callback on gesture progress on the lightwood button
 * Parameters: ClutterGestureAction *, button object reference
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean button_gesture_progress (ClutterGestureAction *pAction, ClutterActor *pButton)
{
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));
	gfloat flPressX, flPressY;
	gfloat motion_x, motion_y;
	gfloat delta_x, delta_y;
	LightwoodSwipeDirection hDirection =  LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN, vDirection =  LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;

        g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pButton), FALSE);

	clutter_click_action_release((ClutterClickAction*)priv->pLongPressAction);

	if(priv->bResetState)
		return FALSE;

	clutter_gesture_action_get_press_coords (pAction,
			0,
			&flPressX,
			&flPressY);

	clutter_gesture_action_get_motion_coords (pAction,
			0,
			&motion_x,
			&motion_y);

	delta_x = flPressX - motion_x;
	delta_y = flPressY - motion_y;

	if (delta_x >= priv->inThreshold)
		hDirection = LIGHTWOOD_SWIPE_DIRECTION_RIGHT;
	else if (delta_x < -priv->inThreshold)
		hDirection = LIGHTWOOD_SWIPE_DIRECTION_LEFT;

	if (delta_y >= priv->inThreshold)
		vDirection = LIGHTWOOD_SWIPE_DIRECTION_DOWN;
	else if (delta_y < -priv->inThreshold)
		vDirection = LIGHTWOOD_SWIPE_DIRECTION_UP;

	/* cancel gesture on direction reversal */
	if (priv->hDirection == LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN)
		priv->hDirection = hDirection;

	if (priv->vDirection == LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN)
		priv->vDirection = vDirection;

	/* swipe should always go to one direction */ 
	/* if swipe starts to  one direction i.e. LIGHTWOOD_SWIPE_DIRECTION_RIGHT
	 *  and during swipe progress direction changes to some other direction,
	 * swipe will get cancelled */
	if (priv->hDirection != hDirection)
	{
		g_warning("hDirection invalid button object\n");
		priv->hDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;
		
		priv->vDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;
		//return FALSE;
	}
	if (priv->vDirection != vDirection)
	{
		g_warning("vDirection invalid button object\n");
		priv->vDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;
		priv->hDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;
		//return FALSE;
	}
	return TRUE;
}

/********************************************************
 * Function : button_gesture_end
 * Description: callback on gesture end
 * Parameters:  ClutterGestureAction *, button object reference,
	      	userData
 * Return value: void
 ********************************************************/
static void button_gesture_end(ClutterGestureAction *pAction, ClutterActor *pButton, gpointer pUserData)
{
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));
	gfloat flPressX, flPressY;
	gfloat flReleaseX, flReleaseY;
	LightwoodSwipeDirection enDirection = LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN;

	//g_assert(LIGHTWOOD_IS_BUTTON (pButton));

	if(priv->bResetState)
		return;

	v_button_stop_reset_timeout(LIGHTWOOD_BUTTON(pButton));	
	
	clutter_gesture_action_get_press_coords (pAction,
			0,
			&flPressX, &flPressY);

	clutter_gesture_action_get_release_coords (pAction,
			0,
			&flReleaseX, &flReleaseY);

	LIGHTWOOD_BUTTON_PRINT("LIGHTWOOD_BUTTON_DEBUG:press X = %f press Y = %f release X = %f release Y = %f\n", flPressX, flPressY, flReleaseX, flReleaseY);

	if ( (flReleaseX - flPressX > priv->inThreshold) 
			|| (flReleaseY - flPressY > priv->inThreshold)
			|| (flPressX - flReleaseX > priv->inThreshold)
			||(flPressY - flReleaseY > priv->inThreshold) )
	{
		gdouble diff, angle;
		/* get the difference of x and y */
		diff = (flReleaseY - flPressY)/ (flReleaseX - flPressX);
		/* calculate the angle */
		angle = atan(diff) * 180 / 3.14159265;

		LIGHTWOOD_BUTTON_PRINT("angle = %f \n", angle);

		/* angles on swipe event 
 
			-90
		  +45	|     -45		 
			|
		-0 _____|_____ +0	
			|	
			|
	          -45	|    +45
			+90
		*/
		/* for up/down direction, angle varies between -45 to -90 and 45 to 90 */
		enDirection = en_get_direction(flPressY, flReleaseY, flPressX, flReleaseX, angle);
	}

	v_handle_gesture_end(LIGHTWOOD_BUTTON(pButton), enDirection);
}

/********************************************************
 * Function : button_long_press
 * Description: callback on button long press
 * Parameters: ClutterClickAction *, button object reference,
	       long press state	
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean button_long_press (ClutterClickAction *action, ClutterActor *pButton, ClutterLongPressState state)
{
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));

        g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pButton), FALSE);

	switch (state)
	{
		case CLUTTER_LONG_PRESS_QUERY:
			LIGHTWOOD_BUTTON_PRINT("LIGHTWOOD_BUTTON_DEBUG: CLUTTER_LONG_PRESS_QUERY:\n");
			/* return TRUE if the actor should support long press
			 * gestures, and FALSE otherwise; this state will be
			 * emitted on button presses
			 */
			if(priv->bReactive && priv->bLongPressEnable)
			{
				v_button_start_reset_timeout(LIGHTWOOD_BUTTON (pButton));
				return TRUE;
			}
			else 
				return FALSE;

		case CLUTTER_LONG_PRESS_ACTIVATE:
			LIGHTWOOD_BUTTON_PRINT("LIGHTWOOD_BUTTON_DEBUG: CLUTTER_LONG_PRESS_ACTIVATE:\n");
			/* this state is emitted if the minimum duration has
			 * been reached without the gesture being cancelled.
			 * the return value is not used
			 */
			if(priv->bReactive && priv->bLongPressEnable)
			{
				if(priv->bResetState == FALSE)
				{
					g_signal_emit (pButton, button_signals[SIG_BUTTON_LONG_PRESS], 0, NULL);

		        		priv->enButtonState = BUTTON_STATE_LONGPRESSED;
					v_button_stop_reset_timeout(LIGHTWOOD_BUTTON (pButton));
				}
			}
			return TRUE;

		case CLUTTER_LONG_PRESS_CANCEL:	
			LIGHTWOOD_BUTTON_PRINT("LIGHTWOOD_BUTTON_DEBUG: CLUTTER_LONG_PRESS_CANCEL:\n");	
			/* this state is emitted if the long press was cancelled;
			 * for instance, the pointer went outside the actor or the
			 * allowed threshold, or the button was released before
			 * the minimum duration was reached. the return value is
			 * not used
			 */
			v_button_stop_reset_timeout(LIGHTWOOD_BUTTON (pButton));
			return TRUE;

		default:
			return TRUE;
	}
	return TRUE;
}

/********************************************************
 * Function : button_clicked
 * Description: callback on click associated with click action
 * Parameters: ClutterClickAction *, button object reference,
		userData
 * Return value: gboolean (handled or not)
 ********************************************************/
static gboolean button_clicked (ClutterClickAction *action, ClutterActor *pButton, gpointer userData)
{
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (LIGHTWOOD_BUTTON (pButton));
	/* if long press state is cancelled, click action will get called  
	   otherwise(in long press activate) not, in that case, button release event will get called */ 

        g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (pButton), FALSE);


	/* if swipe is enabled, no need to emit button release event */ 
        if(priv->bReactive && priv->enButtonState != BUTTON_STATE_SWIPED && priv->enButtonState != BUTTON_STATE_RELEASED)
        {
		if(priv->bResetState == FALSE)
	                g_signal_emit(pButton, button_signals[SIG_BUTTON_RELEASE], 0, NULL);
        }

	v_button_stop_reset_timeout(LIGHTWOOD_BUTTON (pButton));
        priv->enButtonState = BUTTON_STATE_RELEASED;
        return FALSE;
}

/********************************************************
 * Function : lightwood_button_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void lightwood_button_class_init (LightwoodButtonClass *pKlass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
	GParamSpec *pspec = NULL;

	pObjectClass->get_property = lightwood_button_get_property;
	pObjectClass->set_property = lightwood_button_set_property;
	pObjectClass->dispose = lightwood_button_dispose;
	pObjectClass->finalize = lightwood_button_finalize;

	/**
         * LightwoodButton:tooltip-duration:
         *
         * Duration for the tooltip to be shown in msecs
         */
        pspec = g_param_spec_int ("tooltip-duration",
                        "TooltipDuration",
                        "Duration for the tooltip to be shown in msecs",
                        -1, G_MAXINT,
                        3000,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_TOOLTIP_DURATION, pspec);

        /**
         * LightwoodButton:tooltip-enabled:
         *
         * Whether tooltip is enabled
         * Default: FALSE
         */
         pspec = g_param_spec_boolean ("tooltip-enabled",
                        "TooltipEnabled",
                        "Whether the tooltip is enabled",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_TOOLTIP_ENABLED, pspec);


	 /**
         * LightwoodButton:tooltip-direction:
         *
         * To set tooltip direction left/right/up/down
         * Default: left
         */
        pspec = g_param_spec_enum ("tooltip-direction",
                        "TooltipDirection",
                        "Whether the tooltip direction is towards left/right",
                        BUTTON_TYPE_TOOLTIP_DIRECTION,
                        LIGHTWOOD_TOOLTIP_LEFT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_TOOLTIP_DIRECTION, pspec);

	/**
         * LightwoodButton:swipe-enabled:
         *
         * Whether the button swipe action is enable
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("swipe-enabled",
                        "SwipeEnabled",
                        "Whether the button is swipable",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_SWIPE_ENABLED, pspec);

	/**
         * LightwoodButton:reactive:
         *
         * Whether the button is reactive
         * Default:FALSE
	 * To get button events, button needs to be set reactive   
         */
        pspec = g_param_spec_boolean ("reactive",
                        "Reactive",
                        "Whether the button is reactive",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_REACTIVE, pspec);

	/**
         * LightwoodButton:long-press-enabled:
         *
         * Whether the liblightwood-button.has long press enabled
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("long-press-enabled",
                        "LongPressEnabled",
                        "Whether the long press is enabled",
                        FALSE, 
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_LONG_PRESS_ENABLED, pspec);

	/**
         * LightwoodButton:button-reset-duration:
         *
         * button will get reset if
	 * button is not released within the given reset duration
         * Default: > 2000 (greater than long-press-duration if long-press enabled)
         */
        pspec = g_param_spec_int ("button-reset-duration",
                        "ButtonResetDuration",
                        "Duration to reset the button state if not released",
			DEFAULT_RESET_DURATION, G_MAXINT,
                        DEFAULT_RESET_DURATION, 
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_BUTTON_RESET_STATE, pspec);

	// Create a signal for the button to indicate button-press
        /**
         * LightwoodButton::button-press:
         * @lightwoodButton: The object which received the signal
         *
         * button-press is emitted when button is pressed
         */
        button_signals[SIG_BUTTON_PRESS] = g_signal_new ("button-press",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, button_press),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

        // Create a signal for the button to indicate button-release
        /**
         * LightwoodButton::button-release:
         * @lightwoodButton: The object which received the signal
         *
         * button-release is emitted when button is released
         */
        button_signals[SIG_BUTTON_RELEASE] = g_signal_new ("button-release",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, button_release),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	// Create a signal for the button to indicate swipe-direction
        /**
         * LightwoodButton::swipe-action:
         * @lightwoodButton: The object which received the signal
         * @direction: the direction of the action, as a #LightwoodSwipeDirection
         *
         * swipe-action is emitted when button is swipped
         */
        button_signals[SIG_BUTTON_SWIPE_ACTION] = g_signal_new ("swipe-action",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, swipe_action),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__ENUM,
                        G_TYPE_NONE, 1, LIGHTWOOD_BUTTON_TYPE_SWIPE_DIRECTION);

	// Create a signal for the button to indicate long-press
        /**
         * LightwoodButton::long-press:
         * @lightwoodButton: The object which received the signal
         *
         * long press is emitted when button is pressed for a longer time
         */
        button_signals[SIG_BUTTON_LONG_PRESS] = g_signal_new ("button-long-press",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, long_press),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	// Create a signal for the button to indicate tooltip shown
        /**
         * LightwoodButton::tooltip-shown:
         * @lightwoodButton: The object which received the signal
         *
         * tooltip-shown is emitted when button tooltip is shown
         */
        button_signals[SIG_BUTTON_TOOLTIP_SHOWN] = g_signal_new ("tooltip-shown",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, tooltip_shown),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

        // Create a signal for the button to indicate tooltip hide
        /**
         * lightwoodButton::tooltip-hidden:
         * @lightwoodButton: The object which received the signal
         *
         * tooltip-hidden is emitted when buttontooltip is hidden
         */
        button_signals[SIG_BUTTON_TOOLTIP_HIDDEN] = g_signal_new ("tooltip-hidden",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, tooltip_hidden),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

	// Create a signal for the button to indicate button-reset 
        /**
         * LightwoodButton::button-reset:
         * @lightwoodButton: The object which received the signal
         *
         * button-reset is emitted when button is reset
         */
        button_signals[SIG_BUTTON_RESET] = g_signal_new ("button-reset",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (LightwoodButtonClass, button_reset),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0, NULL);

}

/********************************************************
 * Function : lightwood_button_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void lightwood_button_init (LightwoodButton *pButton)
{
	const char *pEnvString;
	LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (pButton);

	/* initialise the priv */
	priv->enButtonState = BUTTON_STATE_RELEASED;
	priv->bLongPressEnable =FALSE;
	priv->bSwipeEnable = FALSE;
	priv->bReactive = FALSE;
	priv->bTooltipEnabled = FALSE;
	priv->bResetState = FALSE;
	priv->inPressTimeoutTimer = 0;
	priv->enLongPressState = CLUTTER_LONG_PRESS_CANCEL;

	priv->inTooltipDuration = -1;
	priv->inResetTimeout = DEFAULT_RESET_DURATION;
	priv->enDirection = -1;

	/* check for env to enable traces */
  	pEnvString = g_getenv ("LIGHTWOOD_BUTTON_DEBUG");
  	if (pEnvString != NULL)
  	{
		button_debug_flags = g_parse_debug_string (pEnvString, button_debug_keys, G_N_ELEMENTS (button_debug_keys));
		LIGHTWOOD_BUTTON_PRINT("env_string %s %u %u \n" , pEnvString , BUTTON_HAS_DEBUG, button_debug_flags);

	}

	/* set properties with default value */
	//g_object_set(pButton, "reactive", TRUE, NULL);

	/* connect signals */
	g_signal_connect (pButton, "button-press-event", G_CALLBACK(event_cb),NULL);
        g_signal_connect (pButton, "button-release-event", G_CALLBACK(event_cb),NULL);
        g_signal_connect (pButton, "touch-event", G_CALLBACK(event_cb),NULL);
	
	/* add gesture action to get swipe event on the lightwood button*/
	/* Inhancement: May be we can implementt a generic swipe gesture */
	priv->pGestureAction = clutter_gesture_action_new ();
	clutter_actor_add_action (CLUTTER_ACTOR (pButton), priv->pGestureAction);
	clutter_gesture_action_set_threshold_trigger_edge (CLUTTER_GESTURE_ACTION (priv->pGestureAction), CLUTTER_GESTURE_TRIGGER_EDGE_AFTER);

	g_signal_connect (priv->pGestureAction, "gesture-begin", G_CALLBACK (button_gesture_begin), NULL);
	g_signal_connect (priv->pGestureAction, "gesture-progress", G_CALLBACK (button_gesture_progress), NULL);
	g_signal_connect (priv->pGestureAction, "gesture-end", G_CALLBACK (button_gesture_end), NULL);

	/* add long press action to the lightwood button */
	priv->pLongPressAction = clutter_click_action_new ();
	clutter_actor_add_action (CLUTTER_ACTOR (pButton), priv->pLongPressAction);
	/* Inhancement: long-press-duration and dnd-drag-threshold should be received through clutter global settings */
	g_object_set (priv->pLongPressAction, "long-press-duration", DEFAULT_LONG_PRESS_DURATION, NULL);
	g_signal_connect (priv->pLongPressAction, "long-press", G_CALLBACK (button_long_press), NULL);
	g_signal_connect (priv->pLongPressAction, "clicked", G_CALLBACK (button_clicked), NULL);

}

/**
 * lightwood_button_set_tooltip_showing:
 * @self: a #LightwoodButton
 * @show: whether tooltip is shown or not
 * 
 * based on tooltip state as show/hide, signal will get emitted  
 */
void
lightwood_button_set_tooltip_showing (LightwoodButton *self,
                                      gboolean show)
{
  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (show)
    g_signal_emit (self, button_signals[SIG_BUTTON_TOOLTIP_SHOWN], 0);
  else
    g_signal_emit (self, button_signals[SIG_BUTTON_TOOLTIP_HIDDEN], 0);
}

/**
 * lightwood_button_get_swipe_enabled:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:swipe-enabled property
 *
 * Returns: the value of #LightwoodButton:swipe-enabled
 */
gboolean
lightwood_button_get_swipe_enabled (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), FALSE);

  return priv->bSwipeEnable;
}

/**
 * lightwood_button_set_swipe_enabled:
 * @self: a #LightwoodButton
 * @enabled: %TRUE to enable swipe, %FALSE to disable it
 *
 * Update the #LightwoodButton:swipe-enabled property
 */
void
lightwood_button_set_swipe_enabled (LightwoodButton *self,
                                    gboolean enabled)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (enabled == priv->bSwipeEnable)
    return;

  priv->bSwipeEnable = enabled;
  g_object_notify (G_OBJECT (self), "swipe-enabled");
}

/**
 * lightwood_button_get_reactive:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:reactive property
 *
 * Returns: the value of #LightwoodButton:reactive
 */
gboolean
lightwood_button_get_reactive (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), FALSE);

  return priv->bReactive;
}

/**
 * lightwood_button_get_long_press_enabled:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:long-press-enabled property
 *
 * Returns: the value of #LightwoodButton:long-press-enabled
 */
gboolean
lightwood_button_get_long_press_enabled (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), FALSE);

  return priv->bLongPressEnable;
}

/**
 * lightwood_button_set_long_press_enabled:
 * @self: a #LightwoodButton
 * @enabled: %TRUE to enable long press support, %FALSE to disable it
 *
 * Update the #LightwoodButton:long-press-enabled property
 */
void
lightwood_button_set_long_press_enabled (LightwoodButton *self,
                                         gboolean enabled)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (enabled == priv->bLongPressEnable)
    return;

  priv->bLongPressEnable = enabled;
  g_object_notify (G_OBJECT (self), "long-press-enabled");
}

/**
 * lightwood_button_get_button_reset_duration:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:button-reset-duration property
 *
 * Returns: the value of #LightwoodButton:button-reset-duration
 */
gint
lightwood_button_get_button_reset_duration (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), 0);

  return priv->inResetTimeout;
}

/**
 * lightwood_button_set_button_reset_duration:
 * @self: a #LightwoodButton
 * @duration: the new duration
 *
 * Update the #LightwoodButton:button-reset-duration property
 */
void
lightwood_button_set_button_reset_duration (LightwoodButton *self,
                                            gint duration)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (duration == priv->inResetTimeout)
    return;

  priv->inResetTimeout = duration;
  g_object_notify (G_OBJECT (self), "button-reset-duration");
}

/**
 * lightwood_button_get_tooltip_duration:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:tooltip-duration property
 *
 * Returns: the value of #LightwoodButton:tooltip-duration
 */
gint
lightwood_button_get_tooltip_duration (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), 0);

  return priv->inTooltipDuration;
}

/**
 * lightwood_button_set_tooltip_duration:
 * @self: a #LightwoodButton
 * @duration: the new duration
 *
 * Update the #LightwoodButton:tooltip-duration property
 */
void
lightwood_button_set_tooltip_duration (LightwoodButton *self,
                                       gint duration)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (duration == priv->inTooltipDuration)
    return;

  priv->inTooltipDuration = duration;
  g_object_notify (G_OBJECT (self), "tooltip-duration");
}

/**
 * lightwood_button_get_tooltip_enabled:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:tooltip-enabled property
 *
 * Returns: the value of #LightwoodButton:tooltip-enabled
 */
gboolean
lightwood_button_get_tooltip_enabled (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), FALSE);

  return priv->bTooltipEnabled;
}

/**
 * lightwood_button_set_tooltip_enabled:
 * @self: a #LightwoodButton
 * @enabled: %TRUE to enable tooltips support
 *
 * Update the #LightwoodButton:tooltip-enabled property
 */
void
lightwood_button_set_tooltip_enabled (LightwoodButton *self,
                                      gboolean enabled)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (enabled == priv->bTooltipEnabled)
    return;

  priv->bTooltipEnabled = enabled;
  g_object_notify (G_OBJECT (self), "tooltip-enabled");
}

/**
 * lightwood_button_get_tooltip_direction:
 * @self: a #LightwoodButton
 *
 * Return the #LightwoodButton:tooltip-direction property
 *
 * Returns: the value of #LightwoodButton:tooltip-direction
 */
LightwoodTooltipDirection
lightwood_button_get_tooltip_direction (LightwoodButton *self)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_val_if_fail (LIGHTWOOD_IS_BUTTON (self), LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN);

  return priv->enDirection;
}

/**
 * lightwood_button_set_tooltip_direction:
 * @self: a #LightwoodButton
 * @direction: the new tooltip direction
 *
 * Update the #LightwoodButton:tooltip-direction property
 */
void
lightwood_button_set_tooltip_direction (LightwoodButton *self,
                                        LightwoodTooltipDirection direction)
{
  LightwoodButtonPrivate *priv = lightwood_button_get_instance_private (self);

  g_return_if_fail (LIGHTWOOD_IS_BUTTON (self));

  if (direction == priv->enDirection)
    return;

  priv->enDirection = direction;
  g_object_notify (G_OBJECT (self), "tooltip-direction");
}
