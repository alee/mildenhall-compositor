/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* lightwood_liblightwood-button.h 
 *
 * Created on: Aug 28, 2012
 * Author: Abhiruchi
 *
 * lightwood_liblightwood-button.h */


#ifndef _LIGHTWOOD_BUTTON_H
#define _LIGHTWOOD_BUTTON_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include <math.h>	

G_BEGIN_DECLS

/* swipe direction as GType */
GType lightwood_button_swipe_direction_get_type (void) G_GNUC_CONST;
#define LIGHTWOOD_BUTTON_TYPE_SWIPE_DIRECTION (lightwood_button_swipe_direction_get_type())

typedef enum _LightwoodSwipeDirection LightwoodSwipeDirection;
typedef enum _LightwoodTooltipDirection LightwoodTooltipDirection;

#define LIGHTWOOD_TYPE_BUTTON (lightwood_button_get_type ())
G_DECLARE_DERIVABLE_TYPE (LightwoodButton, lightwood_button, LIGHTWOOD, BUTTON, ClutterActor)

/**
 * LightwoodButtonClass:
 * @button_press: class handler for the #LightwoodButton::button_press signal
 * @button_release: class handler for the #LightwoodButton::button_release signal
 * @long_press:  class handler for the #LightwoodButton::long_press signal
 * @swipe_action: class handler for the #LightwoodButton::swipe_action signal
 * @tooltip_shown:  class handler for the #LightwoodButton::tooltip_shown signal
 * @tooltip_hidden: class handler for the #LightwoodButton::tooltip_hidden signal
 * @ button_reset: class handler for the #LightwoodButton::button_reset signal
 *
 * The #LightwoodButtonClass struct contains only private data.
 *
 */
struct _LightwoodButtonClass
{
  ClutterActorClass parent_class;

  void (*button_press)    (LightwoodButton *button);
  void (*button_release)  (LightwoodButton *button);
  void (*long_press)      (LightwoodButton *button);
  void (*swipe_action)    (LightwoodButton *button,
                           LightwoodSwipeDirection inDirection);
  void (*tooltip_shown)   (LightwoodButton *button);
  void (*tooltip_hidden)  (LightwoodButton *button);
  void (*button_reset)    (LightwoodButton *button);

  /*<private>*/
  GCallback _padding[8];
};

/* swipe direction on button */
/**
 * LightwoodSwipeDirection
 * @LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN:first value ,0
 * @LIGHTWOOD_SWIPE_DIRECTION_LEFT: Leftwards swipe gesture
 * @LIGHTWOOD_SWIPE_DIRECTION_RIGHT: Rightwards swipe gesture
 * @LIGHTWOOD_SWIPE_DIRECTION_UP: Upwards swipe gesture
 * @LIGHTWOOD_SWIPE_DIRECTION_DOWN: Downwards swipe gesture
 *
 * The main direction of the swipe gesture
 * 
 */
enum _LightwoodSwipeDirection
{
	LIGHTWOOD_SWIPE_DIRECTION_UNKNOWN = 0, 
	LIGHTWOOD_SWIPE_DIRECTION_LEFT 	= 1 << 0,
	LIGHTWOOD_SWIPE_DIRECTION_RIGHT	= 1 << 1,
	LIGHTWOOD_SWIPE_DIRECTION_UP	= 1 << 2,
	LIGHTWOOD_SWIPE_DIRECTION_DOWN	= 1 << 3,
};

/* tooltip direction on button */
/**
 * LightwoodTooltipDirection
 * @LIGHTWOOD_TOOLTIP_LEFT: Left direction for the tooltip
 * @LIGHTWOOD_TOOLTIP_RIGHT: Right direction for the tooltip
 * @LIGHTWOOD_TOOLTIP_UP: up direction for the tooltip
 * @LIGHTWOOD_TOOLTIP_DOWN: down direction for the tooltip
 *
 * The tooltip direction 
 *
 */
enum _LightwoodTooltipDirection
{
        LIGHTWOOD_TOOLTIP_LEFT,
        LIGHTWOOD_TOOLTIP_RIGHT,
        LIGHTWOOD_TOOLTIP_UP,
        LIGHTWOOD_TOOLTIP_DOWN
};

void lightwood_button_set_tooltip_showing (LightwoodButton *self,
                                           gboolean show);

gboolean lightwood_button_get_swipe_enabled (LightwoodButton *self);
void lightwood_button_set_swipe_enabled (LightwoodButton *self,
                                         gboolean enabled);

gboolean lightwood_button_get_reactive (LightwoodButton *self);
void lightwood_button_set_reactive (LightwoodButton *self,
                                    gboolean state);

gboolean lightwood_button_get_long_press_enabled (LightwoodButton *self);
void lightwood_button_set_long_press_enabled (LightwoodButton *self,
                                              gboolean enabled);

gint lightwood_button_get_button_reset_duration (LightwoodButton *self);
void lightwood_button_set_button_reset_duration (LightwoodButton *self,
                                                 gint duration);

gint lightwood_button_get_tooltip_duration (LightwoodButton *self);
void lightwood_button_set_tooltip_duration (LightwoodButton *self,
                                            gint duration);

gboolean lightwood_button_get_tooltip_enabled (LightwoodButton *self);
void lightwood_button_set_tooltip_enabled (LightwoodButton *self,
                                           gboolean enabled);

LightwoodTooltipDirection lightwood_button_get_tooltip_direction (LightwoodButton *self);
void lightwood_button_set_tooltip_direction (LightwoodButton *self,
                                             LightwoodTooltipDirection direction);
G_END_DECLS

#endif /* _LIGHTWOOD_BUTTON_H */
